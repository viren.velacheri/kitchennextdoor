import unittest
import requests
import json


class BackendTests(unittest.TestCase):
    API_URL = "https://api.kitchennextdoor.xyz/"

    def testAPIStatus(self):
        r = requests.get(self.API_URL)
        self.assertEqual(r.status_code, 200)

    def testRecipelist(self):
        r = requests.get(self.API_URL + "recipe")
        self.assertEqual(r.status_code, 200)
        # print(type(r.json()))
        self.assertIsInstance(r.json(), dict)
    
    """ def testRecipeAll(self):
        r = requests.get(self.API_URL + "recipe/all")
        self.assertEqual(r.status_code, 200)
       # print(type(r.json()))
        self.assertIsInstance(r.json(), list) """

    def testRecipeInstance(self):
        r = requests.get(self.API_URL + "recipe/1")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), list)

    def testRecipeInstanceData(self):
        r = requests.get(self.API_URL + "recipe/2")
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json()[0]["aggregateLikes"], 124)

    def testPantryList(self):
        r = requests.get(self.API_URL + "banks")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), dict)
    
    def testPantryAll(self):
        r = requests.get(self.API_URL + "banks/all")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), list)

    def testPantryInstance(self):
        r = requests.get(self.API_URL + "banks/1")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), list)

    def testPantryInstanceData(self):
        r = requests.get(self.API_URL + "banks/2")
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json()[0]["county"], "Salem")

    def testStatsList(self):
        r = requests.get(self.API_URL + "stats")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), dict)
    
    def testStatsAll(self):
        r = requests.get(self.API_URL + "stats/all")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), list)

    def testStatsInstance(self):
        r = requests.get(self.API_URL + "stats/1")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), list)

    def testStatsInstanceData(self):
        r = requests.get(self.API_URL + "stats/2")
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json()[0]["costPerMeal"], "$2.82")

    
    


if __name__ == "__main__":
    unittest.main()
