from flask import Flask
from dbinit import db
import requests
import json, re
import models


def load_recipes():
    file = open("./Data/Recipes/recipedata9.json")
    data = json.load(file)
    recipe_list = []

    for recipe in data["results"]:

        if (
            (len(recipe["dishTypes"]) >= 1)
            & (len(recipe["cuisines"]) >= 1)
            & (len(recipe["diets"]) >= 1)
            & (len(recipe["analyzedInstructions"]) >= 1)
        ):
            equipment_pic = {}
            for step in recipe["analyzedInstructions"][0]["steps"]:
                equipment = step["equipment"]
                for tool in equipment:
                    key = tool["name"]
                    pic = tool["image"]
                    equipment_pic[key] = pic
            pic_list = list(equipment_pic.values())
            if len(pic_list) > 0:

                recipe["summary"] = re.sub("<[^<]+?>", "", recipe["summary"])
                new_recipe = models.Recipe(
                    title=recipe["title"],
                    sourceUrl=recipe["sourceUrl"],
                    image=recipe["image"],
                    summary=recipe["summary"],
                    aggregateLikes=recipe["aggregateLikes"],
                    readyInMinutes=recipe["readyInMinutes"],
                    pricePerServing=recipe["pricePerServing"],
                    healthScore=recipe["healthScore"],
                    analyzedInstructions=recipe["analyzedInstructions"],
                    ingredients=recipe["nutrition"]["ingredients"],
                    cuisines=list(map(str.lower, recipe["cuisines"])),
                    dishTypes=recipe["dishTypes"],
                    diets=recipe["diets"],
                    equipmentPics=pic_list,
                )

                recipe_list.append(new_recipe)
    db.session.add_all(recipe_list)
    db.session.commit()


def load_banks():
    file = open("./Data/FoodBanks/nationalfoodbanksdata.json")
    data = json.load(file)
    banks_list = []
    # print('length of data: ' + str(len(data)))

    for bank in data:
        google_img = (
            "https://maps.googleapis.com/maps/api/place/textsearch/json?query="
            + bank["charityName"]
            + "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4"
        )
        google_response = requests.get(url=google_img, timeout=10)
        # print(len(google_response.json()['results']))
        google_data = google_response.json()["results"][0]
        photo = ""
        if "photos" in google_data.keys():
            photo = google_data["photos"][0]["photo_reference"]
            lat = google_data["geometry"]["location"]["lat"]
            lng = google_data["geometry"]["location"]["lng"]
            google_place = (
                "https://maps.googleapis.com/maps/api/place/photo?photoreference="
                + photo
                + "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4"
            )
        address1 = bank["mailingAddress"]["streetAddress1"]
        address2 = (
            bank["mailingAddress"]["city"]
            + " "
            + bank["mailingAddress"]["stateOrProvince"]
        )

        test_url = "http://www.yaddress.net/api/address?AddressLine1=506+Fourth+Avenue+Unit+1&AddressLine2=Asbury+Prk+NJ"

        county_url = (
            "http://www.yaddress.net/api/address?AddressLine1="
            + address1
            + "&AddressLine2="
            + address2
        )
        county_response = requests.get(url=county_url, timeout=10)
        county = county_response.json()["County"]

        new_bank = models.FoodBank(
            lat=lat,
            lng=lng,
            photo=photo,
            name=bank["charityName"],
            tagline=bank["tagLine"],
            mission=bank["mission"],
            websiteURL=bank["websiteURL"],
            mailing_address=bank["mailingAddress"],
            stateOrProvince=bank["mailingAddress"]["stateOrProvince"],
            county=county,
        )
        banks_list.append(new_bank)
    db.session.add_all(banks_list)
    db.session.commit()


def load_stateData():
    file = open("./Data/CountyStats/statsdata.json")
    data = json.load(file)

    stats_list = []

    state_cuisine = {
        "Alabama": "african",
        "Alaska": "thai",
        "Arizona": "american",
        "Arkansas": "southern",
        "California": "korean",
        "Colorado": "mexican",
        "Connecticut": "mediterranean",
        "Delaware": "chinese",
        "Florida": "greek",
        "Georgia": "caribbean",
        "Hawaii": "thai",
        "Idaho": "mexican",
        "Illinois": "middle eastern",
        "Indiana": "mexican",
        "Iowa": "mexican",
        "Kansas": "mexican",
        "Kentucky": "spanish",
        "Louisiana": "cajun",
        "Maine": "chinese",
        "Maryland": "european",
        "Massachusetts": "chinese",
        "Michigan": "eastern european",
        "Minnesota": "chinese",
        "Mississippi": "mexican",
        "Missouri": "chinese",
        "Montana": "mexican",
        "Nebraska": "mexican",
        "Nevada": "latin american",
        "New Hampshire": "vietnamese",
        "New Jersey": "chinese",
        "New Mexico": "mexican",
        "New York": "jewish",
        "North Carolina": "chinese",
        "North Dakota": "mexican",
        "Ohio": "irish",
        "Oklahoma": "mexican",
        "Oregon": "indian",
        "Pennsylvania": "italian",
        "Rhode Island": "chinese",
        "South Carolina": "chinese",
        "South Dakota": "mexican",
        "Tennessee": "mexican",
        "Texas": "mexican",
        "Utah": "mexican",
        "Vermont": "french",
        "Virginia": "chinese",
        "Washington": "japanese",
        "West Virginia": "chinese",
        "Wisconsin": "mexican",
        "Wyoming": "mexican",
        "District of Columbia": "chinese",
    }

    for state, info in data[0]["states"].items():

        new_stats = models.StateData(
            state=state,
            snap=info["SNAP"],
            foodWorry=info["foodWorry"],
            wentToFoodPantry=info["wentToFoodPantry"],
            cuisine=state_cuisine[state],
        )
        stats_list.append(new_stats)

    db.session.add_all(stats_list)
    db.session.commit()


def load_stats():
    file = open("./Data/CountyStats/statsdata.json")
    data = json.load(file)
    locale_list = []
    counties_list = []

    for county, info in data[1]["counties"].items():
        # Had to go by state one-by-one or else it timed out
        if info["state"] == "District of Columbia":

            google_img = (
                "https://maps.googleapis.com/maps/api/place/textsearch/json?query="
                + county
                + "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4"
            )
            google_response = requests.get(url=google_img, timeout=5)
            google_data = google_response.json()["results"][0]
            photo = ""
            if "photos" in google_data.keys():
                photo = google_data["photos"][0]["photo_reference"]
                lat = google_data["geometry"]["location"]["lat"]
                lng = google_data["geometry"]["location"]["lng"]
                google_place = (
                    "https://maps.googleapis.com/maps/api/place/photo?photoreference="
                    + photo
                    + "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4"
                )

                new_stats = models.Stats(
                    lat=lat,
                    lng=lng,
                    photo=photo,
                    county=county,
                    state=info["state"],
                    snap=info["SNAP"],
                    foodWorry=info["foodWorry"],
                    wentToFoodPantry=info["wentToFoodPantry"],
                    foodInsecurityRate=info["Food Insecurity Rate"],
                    foodInsecurePeople=info["Number of Food Insecure People"],
                    foodInsecurityRateChildren=info[
                        "Food Insecurity Rate for Children"
                    ],
                    foodInsecureChildren=info["Number of Food Insecure Children"],
                    costPerMeal=info["Cost Per Meal"],
                )
                counties_list.append(new_stats)

    locale_list.append(counties_list)
    db.session.add_all(counties_list)
    db.session.commit()


def clean():
    q = db.session.query(models.Stats)

    for domain in q:
        db.session.delete(domain)
    db.session.commit()


if __name__ == "__main__":

    db.create_all()
