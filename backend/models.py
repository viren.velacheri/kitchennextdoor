from dbinit import db, ma
from sqlalchemy.dialects.postgresql import ARRAY


class Recipe(db.Model):
    __tablename__ = "recipe"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    sourceUrl = db.Column(db.String)
    image = db.Column(db.String)
    summary = db.Column(db.String)
    aggregateLikes = db.Column(db.Integer)
    readyInMinutes = db.Column(db.Integer)
    pricePerServing = db.Column(db.Float)
    healthScore = db.Column(db.Integer)
    analyzedInstructions = db.Column(db.JSON)
    ingredients = db.Column(db.JSON)
    cuisines = db.Column(ARRAY(db.String))
    dishTypes = db.Column(ARRAY(db.String))
    diets = db.Column(ARRAY(db.String))
    equipmentPics = db.Column(ARRAY(db.String))

    def __init__(
        self,
        title="NaN",
        sourceUrl="NaN",
        image="NaN",
        summary="NaN",
        aggregateLikes=0,
        readyInMinutes=0,
        pricePerServing=0.0,
        healthScore=0,
        analyzedInstructions="NaN",
        ingredients="NaN",
        cuisines=[],
        dishTypes=[],
        diets=[],
        equipmentPics=[],
    ):
        self.title = title
        self.sourceUrl = sourceUrl
        self.image = image
        self.equipmentPics = equipmentPics
        self.summary = summary
        self.aggregateLikes = aggregateLikes
        self.readyInMinutes = readyInMinutes
        self.pricePerServing = pricePerServing
        self.healthScore = healthScore
        self.analyzedInstructions = analyzedInstructions
        self.ingredients = ingredients
        self.cuisines = cuisines
        self.dishTypes = dishTypes
        self.diets = diets


class RecipeShema(ma.SQLAlchemySchema):
    class Meta:
        # Fields to expose
        fields = (
            "id",
            "title",
            "sourceUrl",
            "image",
            "summary",
            "aggregateLikes",
            "readyInMinutes",
            "pricePerServing",
            "healthScore",
            "analyzedInstructions",
            "ingredients",
            "cuisines",
            "dishTypes",
            "diets",
            "equipmentPics",
        )


recipe_schema = RecipeShema()
recipe_schema = RecipeShema(many=True)


class FoodBank(db.Model):
    __tablename__ = "food bank"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    tagline = db.Column(db.String)
    mission = db.Column(db.String)
    websiteURL = db.Column(db.String)
    mailing_address = db.Column(db.JSON)
    lat = db.Column(db.Float)
    lng = db.Column(db.Float)
    photo = db.Column(db.String)
    stateOrProvince = db.Column(db.String)
    county = db.Column(db.String)

    def __init__(
        self,
        name="NaN",
        stateOrProvince="NaN",
        tagline="Nan",
        mission="NaN",
        websiteURL="NaN",
        mailing_address="NaN",
        lat=0,
        lng=0,
        photo="NaN",
        county="NaN",
    ):
        self.name = name
        self.lat = lat
        self.stateOrProvince = stateOrProvince
        self.lng = lng
        self.photo = photo
        self.tagline = tagline
        self.mission = mission
        self.websiteURL = websiteURL
        self.mailing_address = mailing_address
        self.county = county


class BanksSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "id",
            "name",
            "tagline",
            "mission",
            "websiteURL",
            "mailing_address",
            "lat",
            "lng",
            "photo",
            "stateOrProvince",
            "county",
        )


banks_schema = BanksSchema()
banks_schema = BanksSchema(many=True)


class Stats(db.Model):
    __tablename__ = "stats"

    id = db.Column(db.Integer, primary_key=True)
    county = db.Column(db.String)
    state = db.Column(db.String)
    snap = db.Column(db.String)
    costPerMeal = db.Column(db.String)
    foodInsecureChildren = db.Column(db.String)
    foodInsecurityRateChildren = db.Column(db.String)
    foodInsecurePeople = db.Column(db.String)
    foodInsecurityRate = db.Column(db.String)
    wentToFoodPantry = db.Column(db.String)
    foodWorry = db.Column(db.String)
    lat = db.Column(db.Float)
    lng = db.Column(db.Float)
    photo = db.Column(db.String)

    def __init__(
        self,
        state="NaN",
        snap="NaN",
        county="NaN",
        costPerMeal="NaN",
        foodInsecureChildren="NaN",
        foodInsecurityRateChildren="NaN",
        foodInsecurePeople="NaN",
        foodInsecurityRate="NaN",
        wentToFoodPantry="NaN",
        foodWorry="NaN",
        lat=0,
        lng=0,
        photo="NaN",
    ):
        self.state = state
        self.lat = lat
        self.lng = lng
        self.photo = photo
        self.county = county
        self.snap = snap
        self.costPerMeal = costPerMeal
        self.foodInsecureChildren = foodInsecureChildren
        self.foodInsecurityRateChildren = foodInsecurityRateChildren
        self.foodInsecurePeople = foodInsecurePeople
        self.foodInsecurityRate = foodInsecurityRate
        self.wentToFoodPantry = wentToFoodPantry
        self.foodWorry = foodWorry


class StatsSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "id",
            "state",
            "snap",
            "foodWorry",
            "wentToFoodPantry",
            "lat",
            "lng",
            "photo",
            "county",
            "costPerMeal",
            "foodInsecureChildren",
            "foodInsecurityRateChildren",
            "foodInsecurePeople",
            "foodInsecurityRate",
            "wentToFoodPantry",
            "foodWorry",
        )


stats_schema = StatsSchema()
stats_schema = StatsSchema(many=True)


class StateData(db.Model):
    __tablename__ = "stateData"

    id = db.Column(db.Integer, primary_key=True)
    state = db.Column(db.String)
    snap = db.Column(db.String)
    wentToFoodPantry = db.Column(db.String)
    foodWorry = db.Column(db.String)
    cuisine = db.Column(db.String)

    def __init__(
        self,
        state="NaN",
        snap="NaN",
        wentToFoodPantry="NaN",
        foodWorry="NaN",
        cuisine="NaN",
    ):
        self.state = state
        self.snap = snap
        self.wentToFoodPantry = wentToFoodPantry
        self.foodWorry = foodWorry
        self.cuisine = cuisine


class StateDataSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = ("id", "state", "snap", "foodWorry", "wentToFoodPantry", "cuisine")


stateData_schema = StateDataSchema()
stateData_schema = StateDataSchema(many=True)
