from flask import Flask
from flask import jsonify, request
import urllib.parse
from sqlalchemy import cast, func, or_, String
from werkzeug import Response
from random import randint


import models

from dbinit import app, db

state_abbrev = {
    "Alabama": "AL",
    "Alaska": "AK",
    "Arizona": "AZ",
    "Arkansas": "AR",
    "California": "CA",
    "Colorado": "CO",
    "Connecticut": "CT",
    "Delaware": "DE",
    "Florida": "FL",
    "Georgia": "GA",
    "Hawaii": "HI",
    "Idaho": "ID",
    "Illinois": "IL",
    "Indiana": "IN",
    "Iowa": "IA",
    "Kansas": "KS",
    "Kentucky": "KY",
    "Louisiana": "LA",
    "Maine": "ME",
    "Maryland": "MD",
    "Massachusetts": "MA",
    "Michigan": "MI",
    "Minnesota": "MN",
    "Mississippi": "MS",
    "Missouri": "MO",
    "Montana": "MT",
    "Nebraska": "NE",
    "Nevada": "NV",
    "New Hampshire": "NH",
    "New Jersey": "NJ",
    "New Mexico": "NM",
    "New York": "NY",
    "North Carolina": "NC",
    "North Dakota": "ND",
    "Ohio": "OH",
    "Oklahoma": "OK",
    "Oregon": "OR",
    "Pennsylvania": "PA",
    "Rhode Island": "RI",
    "South Carolina": "SC",
    "South Dakota": "SD",
    "Tennessee": "TN",
    "Texas": "TX",
    "Utah": "UT",
    "Vermont": "VT",
    "Virginia": "VA",
    "Washington": "WA",
    "West Virginia": "WV",
    "Wisconsin": "WI",
    "Wyoming": "WY",
    "District of Columbia": "DC",
    "American Samoa": "AS",
    "Guam": "GU",
    "Northern Mariana Islands": "MP",
    "Puerto Rico": "PR",
    "United States Minor Outlying Islands": "UM",
    "U.S. Virgin Islands": "VI",
}

state_abbrev_rev = dict(map(reversed, state_abbrev.items()))

state_cap = {
    "Alabama": "Jefferson",
    "Alaska": "Anchorage",
    "Arizona": "Maricopa",
    "Arkansas": "Pulaski",
    "California": "Los Angeles",
    "Colorado": "El Paso",
    "Connecticut": "Fairfield",
    "Delaware": "New Castle",
    "Florida": "Miami-Dade",
    "Georgia": "Fulton",
    "Hawaii": "Honolulu",
    "Idaho": "Ada",
    "Illinois": "Cook",
    "Indiana": "Marion",
    "Iowa": "Polk",
    "Kansas": "Johnson",
    "Kentucky": "Jefferson",
    "Louisiana": "East Baton Rouge Parish",
    "Maine": "Cumberland",
    "Maryland": "Montgomery",
    "Massachusetts": "Middlesex",
    "Michigan": "Wayne",
    "Minnesota": "Hennepin",
    "Mississippi": "Hinds",
    "Missouri": "St. Louis",
    "Montana": "Yellowstone",
    "Nebraska": "Douglas",
    "Nevada": "Clark",
    "New Hampshire": "Hillsborough",
    "New Jersey": "Bergen",
    "New Mexico": "Bernalillo",
    "New York": "Kings",
    "North Carolina": "Mecklenberg",
    "North Dakota": "Cass",
    "Ohio": "Cuyahoga",
    "Oklahoma": "Oklahoma",
    "Oregon": "Multnomah",
    "Pennsylvania": "Philadelphia",
    "Rhode Island": "Providence",
    "South Carolina": "Greenville",
    "South Dakota": "Minnehaha",
    "Tennessee": "Shelby",
    "Texas": "Harris",
    "Utah": "Salt Lake",
    "Vermont": "Chittenden",
    "Virginia": "Fairfax",
    "Washington": "King",
    "West Virginia": "Kanawha",
    "Wisconsin": "Milwaukee",
    "Wyoming": "Laramie",
    "District of Columbia": "District of Columbia",
}


@app.route("/")
def hello_world():
    return '<img src="https://i.kym-cdn.com/photos/images/original/001/211/814/a1c.jpg" alt="cowboy" />'


@app.route("/search/<string:search>")
def get_search(search):
    search = search.lower()

    print(search)
    recipe = db.session.query(models.Recipe)
    county = db.session.query(models.Stats)
    bank = db.session.query(models.FoodBank)

    recipe = recipe.filter(
        or_(
            func.lower(models.Recipe.title).contains(search),
            cast(models.Recipe.readyInMinutes, String).contains(search),
            cast(models.Recipe.pricePerServing, String).contains(search),
            cast(models.Recipe.aggregateLikes, String).contains(search),
            cast(models.Recipe.healthScore, String).contains(search),
        )
    )
    county = county.filter(
        or_(
            func.lower(models.Stats.county).contains(search),
            func.lower(models.Stats.state).contains(search),
            func.lower(models.Stats.costPerMeal).contains(search),
            func.lower(models.Stats.foodInsecurePeople).contains(search),
            func.lower(models.Stats.foodInsecurityRate).contains(search),
        )
    )
    bank = bank.filter(
        or_(
            func.lower(models.FoodBank.name).contains(search),
            func.lower(models.FoodBank.stateOrProvince).contains(search),
            func.lower(models.FoodBank.tagline).contains(search),
            func.lower(models.FoodBank.mission).contains(search),
            func.lower(models.FoodBank.websiteURL).contains(search),
        )
    )

    recipe = recipe.paginate(page=1, max_per_page=10)
    county = county.paginate(page=1, max_per_page=10)
    bank = bank.paginate(page=1, max_per_page=10)

    response = {
        "county": models.stats_schema.dump(county.items),
        "recipe": models.recipe_schema.dump(recipe.items),
        "bank": models.banks_schema.dump(bank.items),
    }
    return jsonify(response)


@app.route("/recipe", methods=["GET"])
def get_recipes():

    query = db.session.query(models.Recipe)

    search = request.args.get("search", None, type=str)

    if search:

        search = search.lower()

        query = query.filter(
            or_(
                func.lower(models.Recipe.title).contains(search),
                cast(models.Recipe.readyInMinutes, String).contains(search),
                cast(models.Recipe.pricePerServing, String).contains(search),
                cast(models.Recipe.aggregateLikes, String).contains(search),
                cast(models.Recipe.healthScore, String).contains(search),
            )
        )

    # filters, use args.get and if it exists, filter the query
    diet = request.args.get("diet", None, type=str)

    print(diet)
    if diet:
        print(diet)

        dietfilt = "{" + diet + "}"

        query = query.filter(models.Recipe.diets.contains(dietfilt))

    dishType = request.args.get("dishType", None, type=str)
    if dishType:
        print(dishType)
        dishfilt = "{" + dishType + "}"
        query = query.filter(models.Recipe.dishTypes.contains(dishfilt))

    cuisine = request.args.get("cuisine", None, type=str)
    if cuisine:
        print(cuisine)
        cuisinefilt = "{" + cuisine + "}"
        query = query.filter(models.Recipe.cuisines.contains(cuisinefilt))

    sort = request.args.get("sort", None, type=str)

    if sort:
        print(sort)
        if sort == "aggregateLikes":
            query = query.order_by(models.Recipe.aggregateLikes)
        if sort == "healthScore":
            query = query.order_by(models.Recipe.healthScore)
    # pagination, ("key", default, type)
    page = request.args.get("page", 1, type=int)
    numPerPage = request.args.get("numPerPage", 5, type=int)
    if numPerPage == 0:
        numPerPage = 5

    recipes = query.paginate(page=page, max_per_page=numPerPage)
    totalNumPages = recipes.pages
    response = {
        "pages": totalNumPages,
        "data": models.recipe_schema.dump(recipes.items),
    }
    return jsonify(response)


@app.route("/recipe/<int:id>", methods=["GET"])
def get_recipe(id):
    recipe = models.Recipe.query.filter_by(id=id)
    response = models.recipe_schema.dump(recipe)
    print(jsonify(response))
    return jsonify(response)


@app.route("/recipe/<string:state>", methods=["GET"])
def get_recipe_cuisine(state):
    if len(state) == 2:
        state = state_abbrev_rev[state]

    states = models.StateData.query.filter(state == state)

    page = randint(1, states.count())
    print(page)

    states = states.paginate(page=page, max_per_page=1)

    response = models.stateData_schema.dump(states.items)
    cuisine = response[0]["cuisine"]
    query = db.session.query(models.Recipe)
    cuisinefilt = "{" + cuisine + "}"
    query = query.filter(models.Recipe.cuisines.contains(cuisinefilt))
    query = query.all()
    response1 = models.recipe_schema.dump(query)
    return jsonify(response1)


@app.route("/banks", methods=["GET"])
def get_foodbanks():

    query = db.session.query(models.FoodBank)

    search = request.args.get("search", None, type=str)

    if search:

        search = search.lower()

        query = query.filter(
            or_(
                func.lower(models.FoodBank.name).contains(search),
                func.lower(models.FoodBank.stateOrProvince).contains(search),
                func.lower(models.FoodBank.tagline).contains(search),
                func.lower(models.FoodBank.mission).contains(search),
                func.lower(models.FoodBank.websiteURL).contains(search),
            )
        )

    # filters, use args.get and if it exists, filter the query
    state = request.args.get("state", None, type=str)

    if state:
        print(state)

        query = query.filter(models.FoodBank.stateOrProvince == state)

    sort = request.args.get("sort", None, type=str)

    if sort:
        print(sort)
        if sort == "tagline":
            query = query.order_by(models.FoodBank.tagline)
        if sort == "mission":
            query = query.order_by(models.FoodBank.mission)
        if sort == "websiteURL":
            query = query.order_by(models.FoodBank.websiteURL)
        if sort == "name":
            query = query.order_by(models.FoodBank.name)

    # pagination, ("key", default, type)
    page = request.args.get("page", 1, type=int)
    numPerPage = request.args.get("numPerPage", 5, type=int)
    if numPerPage == 0:
        numPerPage = 5

    banks = query.paginate(page=page, max_per_page=numPerPage)
    totalNumPages = banks.pages

    response = {"pages": totalNumPages, "data": models.banks_schema.dump(banks.items)}

    return jsonify(response)


@app.route("/banks/<int:id>", methods=["GET"])
def get_foodbank(id):
    bank = models.FoodBank.query.filter_by(id=id)
    response = models.banks_schema.dump(bank)
    return jsonify(response)


@app.route("/banks/<string:state>/<string:county>", methods=["GET"])
def get_foodbank_county_connection(state, county):

    act_state = state_abbrev[state]

    query = db.session.query(models.FoodBank)
    state_q = query.filter(models.FoodBank.stateOrProvince == state_abbrev[state])
    if state_q.count() == 0:
        default = query.filter(models.FoodBank.stateOrProvince == "TX")
        default = default.filter(models.FoodBank.county == "Travis")
        default = default.all()
        response = models.banks_schema.dump(default)
        return jsonify(response)

    county_q = query.filter(models.FoodBank.county == county)

    if county_q.count() == 0:
        try_cap = query.filter(models.FoodBank.county == state_cap[state])
        if try_cap.count == 1:
            try_cap = try_cap.all()
            response = models.banks_schema.dump(try_cap)
            return jsonify(response)

        first_valid = state_q.all()

        response = models.banks_schema.dump(first_valid)
        return jsonify(response)
    bank = query.all()

    response = models.banks_schema.dump(bank)
    return jsonify(response)


@app.route("/stats", methods=["GET"])
def get_Stats():

    query = db.session.query(models.Stats)
    search = request.args.get("search", None, type=str)
    if search:

        search = search.lower()
        print("SERACH " + search)
        query = query.filter(
            or_(
                func.lower(models.Stats.county).contains(search),
                func.lower(models.Stats.state).contains(search),
                func.lower(models.Stats.costPerMeal).contains(search),
                func.lower(models.Stats.foodInsecurePeople).contains(search),
                func.lower(models.Stats.foodInsecurityRate).contains(search),
            )
        )

    # filters, use args.get and if it exists, filter the query
    state = request.args.get("state", None, type=str)

    if state:
        print(state)
        query = query.filter(models.Stats.state == state)

    sort = request.args.get("sort", None, type=str)

    if sort:
        print(sort)
        if sort == "foodInsecureChildren":
            query = query.order_by(models.Stats.foodInsecureChildren)
        if sort == "foodInsecurePeople":
            query = query.order_by(models.Stats.foodInsecurePeople)
        if sort == "costPerMeal":
            query = query.order_by(models.Stats.costPerMeal)
        if sort == "foodInsecurityRate":
            query = query.order_by(models.Stats.foodInsecurityRate)

    # pagination, ("key", default, type)
    page = request.args.get("page", 1, type=int)
    numPerPage = request.args.get("numPerPage", 5, type=int)
    if numPerPage == 0:
        print(numPerPage)
        numPerPage = 5

    stats = query.paginate(page=page, max_per_page=numPerPage)
    totalNumPages = stats.pages

    response = {"pages": totalNumPages, "data": models.stats_schema.dump(stats.items)}

    return jsonify(response)


@app.route("/stats/<int:id>", methods=["GET"])
def get_StatByState(id):
    stat = models.Stats.query.filter_by(id=id)
    response = models.stats_schema.dump(stat)
    return jsonify(response)


@app.route("/stats/<string:state>/<string:county>", methods=["GET"])
def get_StatByStateConnection(state, county):

    act_state = state_abbrev_rev[state]

    query = db.session.query(models.Stats)
    query = query.filter(models.Stats.state == act_state)
    query = query.filter(models.Stats.county.contains(county))

    stat = query.all()

    response = models.stats_schema.dump(stat)
    return jsonify(response)


@app.route("/banks/recipe/<string:cuisine>", methods=["GET"])
def get_bank_by_cuisine(cuisine):
    states = models.StateData.query.filter_by(cuisine=str(cuisine))
    page = randint(1, states.count())
    print(page)

    states = states.paginate(page=page, max_per_page=1)

    response = models.stateData_schema.dump(states.items)
    state_name = response[0]["state"]

    state_name = state_abbrev[state_name]
    query = db.session.query(models.FoodBank)
    query = query.filter(models.FoodBank.stateOrProvince == state_name)
    print(query.count())
    if query.count() == 0:
        texas = db.session.query(models.FoodBank)
        texas = texas.filter(models.FoodBank.stateOrProvince == "TX")
        print(texas.count())
        texas = texas.all()
        tex_response = models.banks_schema.dump(texas)
        return jsonify(tex_response)

    query = query.all()

    response_con = models.banks_schema.dump(query)

    return jsonify(response_con)


@app.route("/stats/recipe/<string:cuisine>", methods=["GET"])
def get_county_by_cuisine(cuisine):
    states = models.StateData.query.filter_by(cuisine=str(cuisine))
    page = randint(1, states.count())
    print(page)

    states = states.paginate(page=page, max_per_page=1)

    response = models.stateData_schema.dump(states.items)
    state_name = response[0]["state"]

    query = db.session.query(models.Stats)
    query = query.filter(models.Stats.state == state_name)

    county = state_cap[state_name]
    print(county)

    query = query.filter(models.Stats.county.contains(county))

    query = query.all()

    response_con = models.stats_schema.dump(query)

    return jsonify(response_con)


@app.route("/recipe/all", methods=["GET"])
def get_all_recipe():
    query = models.Recipe.query.all()
    response = models.recipe_schema.dump(query)
    return jsonify(response)


@app.route("/stats/all", methods=["GET"])
def get_all_stats():
    query = models.Stats.query.all()
    response = models.stats_schema.dump(query)
    return jsonify(response)


@app.route("/banks/all", methods=["GET"])
def get_all_banks():
    query = models.FoodBank.query.all()
    response = models.banks_schema.dump(query)
    return jsonify(response)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
