import requests
import json


api_url = "https://api.spoonacular.com/recipes/complexSearch"

params = {
    "apiKey": "69fccd96759b4de1be07eaac73adcf66",
    "addRecipeInformation": "true",
    "addRecipeNutrition": "true",
    "number": "100",
    "offset": 600,
}

response = requests.get(url=api_url, params=params)
with open("recipedata6.json", "w") as outfile:
    outfile.write(response.text)
