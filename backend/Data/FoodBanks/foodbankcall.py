from flask import request
import requests
import json


api_url = "https://api.data.charitynavigator.org/v2/Organizations?app_id=a06e8932&app_key=b003924d0e7661cb18508a6079e024cb&search=food%20bank&state=TX"

params = {
    "app_id": "69fccd96759b4de1be07eaac73adcf66",
    "addRecipeInformation": "true",
    "addRecipeNutrition": "true",
}

response = requests.get(url=api_url)
google_img = (
    "https://maps.googleapis.com/maps/api/place/textsearch/json?query="
    + "Harris County"
    + "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4"
)
google_response = requests.get(url=google_img)
google_data = google_response.json()["results"][0]

photo = google_data["photos"][0]["photo_reference"]
lat = google_data["geometry"]["location"]["lat"]
lng = google_data["geometry"]["location"]["lng"]
google_place = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photo_reference=Aap_uEA7vb0DDYVJWEaX3O-AtYp77AaswQKSGtDaimt3gt7QCNpdjp1BkdM6acJ96xTec3tsV_ZJNL_JP-lqsVxydG3nh739RE_hepOOL05tfJh2_ranjMadb3VoBYFvF0ma6S24qZ6QJUuV6sSRrhCskSBP5C1myCzsebztMfGvm7ij3gZT&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4"
google = requests.get(url=google_place)
print(google.json())

# with open("foodbankdata.json", "w") as outfile:
# outfile.write(response.text)
