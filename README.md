# Kitchennextdoor

| Name            | UTEID   | GitLab ID        |
| --------------- | ------- | ---------------- |
| Viren Velacheri | vv6898  | viren.velacheri  |
| Nicholas Ehlers | nwe249  | nicholas.ehlers1 |
| Michael Tran    | jt42943 | jmtran678        |
| Antoney Ejimadu | aee778  | toney.ejimadu1   |
| Kevin Joseph    | kj23759 | kj3818           |

## Git SHA

Phase 1 - fab0490ca736f317b11ca452b5419f5c7a56415f

Phase 2 - 77d662defe81e4f22f7281d50577311d464b5560

Phase 3 - 201707cf55626480122026177c3d37261ec062dc

Phase 4 - 711a5a1684b2a863c5d494f61355425afa178682

## Project Leader

Phase 1 - Viren Velacheri

Phase 2 - Michael Tran

Phase 3 - Antoney Ejimadu

Phase 4 - Nicholas Ehlers

What Leader should do:

- Be the main person in distributing tasks/issues to others
- Check in with everyone to see if any problems have arisen
- When an issue comes up, leader should be swift in coming up with a plan to resolve issue
- Oversee everything
- Get others involved as necessary

## Gitlab Pipeline

Phase 1 - https://gitlab.com/viren.velacheri/kitchennextdoor/-/pipelines

Phase 2 - https://gitlab.com/viren.velacheri/kitchennextdoor/-/pipelines

Phase 3 - https://gitlab.com/viren.velacheri/kitchennextdoor/-/pipelines

Phase 4 - https://gitlab.com/viren.velacheri/kitchennextdoor/-/pipelines

## Website Link

https://www.kitchennextdoor.xyz

## Presentation Video Link
https://www.youtube.com/watch?v=9sn-_wVMdH4&t=4s 

## Estimated Completion Time

### Phase 1

| Name            | Estimated Time (hrs) |
| --------------- | -------------------- |
| Viren Velacheri | 10                   |
| Nicholas Ehlers | 10                   |
| Michael Tran    | 12                   |
| Antoney Ejimadu | 12                   |
| Kevin Joseph    | 10                   |

## Actual Completion Time

| Name            | Actual Time (hrs) |
| --------------- | ----------------- |
| Viren Velacheri | 20                |
| Nicholas Ehlers | 16                |
| Michael Tran    | 15                |
| Antoney Ejimadu | 25                |
| Kevin Joseph    | 20                |

### Phase 2

| Name            | Estimated Time (hrs) |
| --------------- | -------------------- |
| Viren Velacheri | 18                   |
| Nicholas Ehlers | 20                   |
| Michael Tran    | 25                   |
| Antoney Ejimadu | 30                   |
| Kevin Joseph    | 25                   |

## Actual Completion Time

| Name            | Actual Time (hrs) |
| --------------- | ----------------- |
| Viren Velacheri | 30                |
| Nicholas Ehlers | 25                |
| Michael Tran    | 35                |
| Antoney Ejimadu | 30                |
| Kevin Joseph    | 32                |

## Phase 3

| Name            | Estimated Time (hrs) |
| --------------- | -------------------- |
| Viren Velacheri | 25                   |
| Nicholas Ehlers | 25                   |
| Michael Tran    | 30                   |
| Antoney Ejimadu | 20                   |
| Kevin Joseph    | 30                   |

## Actual Completion Time

| Name            | Actual Time (hrs) |
| --------------- | ----------------- |
| Viren Velacheri | 35                |
| Nicholas Ehlers | 30                |
| Michael Tran    | 35                |
| Antoney Ejimadu | 25                |
| Kevin Joseph    | 34                |

## Phase 4

| Name            | Estimated Time (hrs) |
| --------------- | -------------------- |
| Viren Velacheri | 5                    |
| Nicholas Ehlers | 5                    |
| Michael Tran    | 10                   |
| Antoney Ejimadu | 8                    |
| Kevin Joseph    | 9                    |

## Actual Completion Time

| Name            | Actual Time (hrs) |
| --------------- | ----------------- |
| Viren Velacheri | 7                 |
| Nicholas Ehlers | 4                 |
| Michael Tran    | 8                 |
| Antoney Ejimadu | 10                |
| Kevin Joseph    | 5                 |

## Comments

### Phase 1 Comments

We looked at a couple of groups such as Bookipedia for seeing how to go about

doing the Makefile as well as the gitlab continuous integration pipeline file.

### Phase 2 Comments

We looked at groups Just a Bill and College Football in reference to Selenium and Jest Tests respectively.
Also looked College Football group as reference for Python Unit tests.

## Phase 3 Comments

Looked at Just a Bill and College Football groups in reference to how to run the Postman unit tests and Jest tests.

## Phase 4 Comments

Looked at group ATravelTexas (https://gitlab.com/BWN133/atraveltexas/) to see an example of how to do the data visualizations.

# RFP

## Project Intro

- Canvas / Discord group number: 11am, Group #4

- Names of the team members: Viren Velacheri, Nicholas Ehlers, Antoney Ejimadu, Kevin Joseph, Michael Tran

- name of the project (alphanumeric, no spaces, max 32 chars, this will also be your URL): KitchenNextDoor

- URL of the GitLab repo: https://gitlab.com/viren.velacheri/kitchennextdoor

- the proposed project: A website that not only helps people find food pantries but also explains the severity of related problems based on statistics such as hunger rates as well as recipes that people can make to donate.

- URLs of at least three disparate data sources that you will programmatically scrape using a RESTful API (be very sure about this):

https://www.centraltexasfoodbank.org/

http://hopefoodpantryaustin.org/

https://deanofstudents.utexas.edu/emergency/utoutpost.php

https://foodpantries.org/

https://map.feedingamerica.org/county/2019/overall/texas

https://www.themealdb.com/api.php

- at least three models:

Food Pantry

City Statistics

Recipes

- an estimate of the number of instances of each model:

Number of pantries in Texas: around 700

Number of statistics per top 50 cities: 200+

Number of recipes: 100+

- each model must have many attributes:

        Food Pantry:

            - Hours open

            - Popularity Rating

            - Number of volunteers

            - Location

            - Number of meals provided

            - Name of Food Pantry

            - Mission statement

            - Pantry Description

            - Sponsors

            - Year opened


        City Statistics:

            - location

            - type of statistics (food and security, number of students that qualify for reduced lunch, pounds of food waste, etc)

            - population

            - county

            - political district

            - name of the city

            - mayor of the city

            - zipcode

            - famous landmarks

            - neighborhoods

        Recipes:

            - Ingredients

            - Cuisine

            - Dietary restrictions

            - Time to make

            - Meal (breakfast, lunch, snack, or dinner)

            - Name

            - Food Culture

            - Writer of the recipe

            - Meal (breakfast, lunch, snack, or dinner)

            - Dietary restrictions

- describe at least five of those attributes for each model that you could filter by or sort by on the model (table) pages:

First five attributes of each of the models are the sortable ones

- describe at least five additional attributes for each model that you could search for on the instance pages:

Last five attributes of each of the models are the ones you could search for on instance pages

- each model must connect to at least two other models:

        Food Pantry

            - Food pantries are a part of cities and provide to those in hunger

            - Food pantries make use of dishes/food that are made based on recipes

        City statistics

            - Cities have food pantries and are known for meals and recipes

        Recipes

            - Recipes make food which can be given to food pantries or cities that have people in need of hunger

- each model must be rich with different media (e.g., feeds, images, maps, text, videos, etc.) (be very sure about this):

        Food Pantry:

            Logo/Symbol (image)

            Embedded map of location (maps)

            Links to social media (text)

        City Statistics:

            Statistic Information (text)

            Picture of iconic landmark (image)

        Recipes:

            Food (image)

            Instructions (text)

            How to make videos (video)

- describe at least two types of media for each model that you could display on the instance pages:

see above

- what three questions will you answer due to doing this data synthesis on your site?:

1. What are some nearby food pantries that offer food to the hungry?

2. What are food insecurity rates in the cities and the contributing factors?

3. What are meals that can be made at home and be given out to food pantries?
