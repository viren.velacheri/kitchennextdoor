import pytest
from selenium.webdriver import Chrome, ChromeOptions
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Remote
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture
def browser():
    options = ChromeOptions()

    # add options to run in headless mode (no gui)
    options.add_argument("--no-sandbox")
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("--disable-dev-shm-usage")

    # install and init chrome driver
    # driver = Chrome(options=options, service=Service(ChromeDriverManager().install()))
    driver = Remote(
            "http://selenium__standalone-chrome:4444/wd/hub",
            desired_capabilities=options.to_capabilities(),
        )

    # wait up to 10 seconds before throwing an exception
    driver.implicitly_wait(10)

    # provide tests with the driver
    yield driver

    # cleanup
    driver.quit()
