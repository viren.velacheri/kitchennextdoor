from selenium.webdriver.common.by import By
from time import sleep

HOST_NAME = "https://development.d35kugx2rd905c.amplifyapp.com/"

class TestGUI:
    def test_app(self, browser):
        browser.get(HOST_NAME)
        app = browser.find_element(by=By.CSS_SELECTOR, value=".Div")
        assert app != None

    # Test NavBar Links
    def test_about_link(self, browser):
        browser.get(HOST_NAME)
        about_link = browser.find_element(By.LINK_TEXT, value="About")
        about_link.click()
        sleep(3)
        browser.refresh()
        about_page = browser.find_element(By.CSS_SELECTOR, value=".bigText")
        about_title = browser.find_element(By.CSS_SELECTOR, value="p")
        assert about_page != None
        assert about_title.text == 'About Us'

    def test_pantries_link(self, browser):
        browser.get(HOST_NAME)
        pantries_link = browser.find_element(By.LINK_TEXT, value="Find a Pantry")
        pantries_link.click()
        sleep(3)
        browser.refresh()
        pantries_page = browser.find_element(By.CSS_SELECTOR, value=".PantryBody")
        # pantries_title = browser.find_element(By.TAG_NAME, value="Typography")
        assert pantries_page != None
        # assert "Find the pantries closest to you!" in pantries_title.text

    def test_statistics_link(self, browser):
        browser.get(HOST_NAME)
        stats_link = browser.find_element(By.LINK_TEXT, value="Hunger Statistics")
        stats_link.click()
        sleep(3)
        browser.refresh()
        stats_page = browser.find_element(By.CSS_SELECTOR, value=".StatsBody")
        # stats_title = browser.find_element(By.TAG_NAME, value="Typography")
        assert stats_page != None
        # assert  "Learn more about how hunger is impacting counties throughout the US" in stats_title.text

    def test_recipes_link(self, browser):
        browser.get(HOST_NAME)
        recipes_link = browser.find_element(By.LINK_TEXT, value="Recipes")
        recipes_link.click()
        sleep(3)
        browser.refresh()
        recipes_page = browser.find_element(By.CSS_SELECTOR, value=".RecipeBody")
        # recipes_title = browser.find_element(By.TAG_NAME, value="Typography")
        assert recipes_page != None
        # assert "Find recipes to prepare and donate" in recipes_title.text

    def test_home_link(self, browser):
        browser.get(HOST_NAME)
        home_link = browser.find_element(By.LINK_TEXT, value="KitchenNextDoor")
        home_link.click()
        sleep(3)
        browser.refresh()
        home_page = browser.find_element(By.CSS_SELECTOR, value=".DivLeft")
        home_title = browser.find_element(By.TAG_NAME, value="p")
        assert home_page != None
        assert "In Need of a Meal?" in home_title.text

    # Test Model Pages have Cards
    def test_about_page(self, browser):
        browser.get(HOST_NAME + 'about')
        about_cards = browser.find_elements(By.CSS_SELECTOR, value=".card-body")
        assert "Viren Velacheri" in about_cards[0].text
        assert "Michael Tran" in about_cards[1].text
        assert "Nicholas Ehlers" in about_cards[2].text
        assert "Antoney Ejimadu" in about_cards[3].text
        assert "Kevin Joseph" in about_cards[4].text
    
    def test_pantries_sample_instance(self, browser):
        browser.get(HOST_NAME + 'banks/view/1')
        pantries_instance_page = browser.find_element(By.CSS_SELECTOR, value=".PantryInstance")
        assert pantries_instance_page != None
        
    def test_stats_sample_instance(self, browser):
        browser.get(HOST_NAME + 'stats/view/1')
        stats_instance_page = browser.find_element(By.CSS_SELECTOR, value=".StatsInstance")
        assert stats_instance_page != None
    
    def test_recipes_sample_instance(self, browser):
        browser.get(HOST_NAME + 'recipes/view/1')
        recipes_instance_page = browser.find_element(By.CSS_SELECTOR, value=".RecipeInstance")
        assert recipes_instance_page != None


