import React, { useEffect, useState } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ZAxis,
  ResponsiveContainer,
} from "recharts"; // bar chart
import { ScatterChart, Scatter } from "recharts"; // scatter chart
import { PieChart, Pie } from "recharts"; // pie chart
import VisArray from "../Interfaces/IVis";
import axios from "axios";
import Typography from "@mui/material/Typography";
import "../Style/VisualizationPageLanding.css";

let PROVIDER_ENDPOINT = "https://api.animalwatch.net";

// provider visualizations
function getParksData(parksData: any) {
  // scatter plot
  var data: { [key: number]: { [keyy: string]: any } } = parksData["page"];
  var returnVal: { cost: number; activities: number }[] = [];

  for (var idx in data) {
    var activities: string = data[idx]["activities"];
    var cost: string = data[idx]["admission_cost"];
    var park: string = data[idx]["name"];
    var tmpDict: { cost: number; activities: number; park: string } = {
      cost: 0,
      activities: 0,
      park: "",
    };
    tmpDict.cost = parseInt((cost.match(/\d+/) || ["0"])[0], 10);
    tmpDict.activities = (activities.match(/,/g) || []).length;
    tmpDict.park = park;
    returnVal.push(tmpDict);
  }
  returnVal = returnVal.sort((a, b) => a.cost - b.cost);
  return returnVal;
}

function getSpeciesData(speciesData: any) {
  // bar chart
  var data: { [key: number]: { [keyy: string]: any } } = speciesData["page"];
  var phylum_map: { [key: string]: number } = {};
  var returnVal: { phylum: string; amt: number }[] = [];

  for (var idx in data) {
    var phylum = data[idx]["tax_phylum"];
    if (phylum_map[phylum] == undefined) {
      phylum_map[phylum] = 1;
    } else {
      phylum_map[phylum] += 1;
    }
  }

  for (var currPhylum in phylum_map) {
    var tmpDict: { phylum: string; amt: number } = {
      phylum: "",
      amt: 0,
    };
    tmpDict.phylum = currPhylum;
    tmpDict.amt = phylum_map[currPhylum];
    returnVal.push(tmpDict);
  }
  return returnVal;
}

function getRecreationData(recData: any) {
  var data: { [key: number]: { [keyy: string]: any } } = recData["page"];
  var reservable_map: { [key: string]: number } = {};
  var returnVal: { ans: string; amt: number }[] = [];

  for (var idx in data) {
    var ans = data[idx]["reservable"];
    if (reservable_map[ans] == undefined) {
      reservable_map[ans] = 1;
    } else {
      reservable_map[ans] += 1;
    }
  }

  for (var currAns in reservable_map) {
    var tmpDict: { ans: string; amt: number } = {
      ans: "",
      amt: 0,
    };
    tmpDict.ans = currAns;
    tmpDict.amt = reservable_map[currAns];
    returnVal.push(tmpDict);
  }
  return returnVal;
}

function ProviderVisualizationPageLanding() {
  const [parksData, setParksData] = React.useState<[]>([]);
  const [speciesData, setSpeciesData] = React.useState<[]>([]);
  const [recData, setRecData] = React.useState<[]>([]);
  const [loading, setLoading] = React.useState(true);

  useEffect(() => {
    const parksUrl = PROVIDER_ENDPOINT + "/parks";
    axios
      .get(parksUrl)
      .then(function (response) {
        console.log(response);
        setParksData(response.data);

        // handle succes
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }, []);

  useEffect(() => {
    const speciesUrl = PROVIDER_ENDPOINT + "/species";
    axios
      .get(speciesUrl)
      .then(function (response) {
        console.log(response);
        setSpeciesData(response.data);

        // handle succes
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }, []);

  useEffect(() => {
    const recUrl = PROVIDER_ENDPOINT + "/recreation";
    axios
      .get(recUrl)
      .then(function (response) {
        console.log(response);
        setRecData(response.data);

        // handle succes
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }, []);

  const [parksVisData, setParksVis] = useState<VisArray[]>([]);
  useEffect(() => {
    let vis = getParksData(parksData);
    console.log(parksData);
    setParksVis(vis);
    setLoading(false);
  }, [parksData]);

  const [speciesVisData, setSpeciesVis] = useState<VisArray[]>([]);
  useEffect(() => {
    let vis = getSpeciesData(speciesData);
    console.log(vis);
    setSpeciesVis(vis);
    setLoading(false);
  }, [speciesData]);

  const [recVisData, setRecVis] = useState<VisArray[]>([]);
  useEffect(() => {
    let vis = getRecreationData(recData);
    console.log(vis);
    setRecVis(vis);
    setLoading(false);
  }, [recData]);

  return (
    <div className="VisualizationBody">
      <Typography variant="h1" gutterBottom component="div">
        Provider Visualizations
      </Typography>

      <Typography variant="h3" gutterBottom component="div">
        Comparing Admission Cost to Amount of activities
      </Typography>

      {loading ? (
        console.log("loading")
      ) : (
        <div className="ChartWrapper">
          <ResponsiveContainer>
            <ScatterChart margin={{ top: 20, right: 20, bottom: 10, left: 10 }}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="cost" name="Cost" unit="$" />
              <YAxis dataKey="activities" name="Activities" />
              <ZAxis dataKey="park" name="Parks" />

              <Tooltip cursor={{ strokeDasharray: "3 3" }} />
              <Legend />
              <Scatter name="Park" data={parksVisData} fill="#f9a826" />
            </ScatterChart>
          </ResponsiveContainer>
        </div>
      )}

      <Typography variant="h3" gutterBottom component="div">
        The Phylum of Recorded Species in API
      </Typography>

      {loading ? (
        console.log("loading")
      ) : (
        <div className="ChartWrapper">
          <ResponsiveContainer>
            <BarChart data={speciesVisData}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="phylum" name="Phylum" />
              <YAxis dataKey="amt" />
              <Tooltip />
              <Legend />
              <Bar
                dataKey="amt"
                fill="#f9a826"
                name="Number of Species in this Phylum"
              />
            </BarChart>
          </ResponsiveContainer>
        </div>
      )}

      <Typography variant="h3" gutterBottom component="div">
        Recreation Spots That Are Reservable
      </Typography>
      {loading ? (
        console.log("loading")
      ) : (
        <div className="ChartWrapper">
          <ResponsiveContainer>
            <PieChart>
              <Pie
                data={recVisData}
                dataKey="amt"
                nameKey="ans"
                fill="#f9a826"
                label
              />
              <Tooltip />
            </PieChart>
          </ResponsiveContainer>
        </div>
      )}
    </div>
  );
}
export default ProviderVisualizationPageLanding;
