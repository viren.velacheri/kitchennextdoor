import React, { useEffect, useState } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ZAxis,
  ScatterChart,
  Scatter,
  PieChart,
  Pie,
  ResponsiveContainer,
} from "recharts";
import IRecipeArray from "../../RecipesPage/Interfaces/IRecipe";
import IPantryArray from "../../PantryPage/Interfaces/IPantry";
import IStatsArray from "../../StatsPage/Interfaces/IStats";
import VisArray from "../Interfaces/IVis";
import { API_ENDPOINT } from "../../GlobalConsts";
import axios from "axios";
import Typography from "@mui/material/Typography";
import "../Style/VisualizationPageLanding.css";

function getStatsData(statsData: IStatsArray[]) {
  var foodInsecurePeoplePerStateData: { [key: string]: number } = {};
  var data: { [key: number]: { [keyy: string]: any } } = statsData;

  for (var idx in data) {
    var state = data[idx]["state"];
    if (foodInsecurePeoplePerStateData[state] == undefined) {
      foodInsecurePeoplePerStateData[state] = parseInt(
        data[idx]["foodInsecurePeople"],
        10
      );
    } else {
      foodInsecurePeoplePerStateData[state] += parseInt(
        data[idx]["foodInsecurePeople"],
        10
      );
    }
  }

  var returnVal: VisArray[] = [];
  for (var currState in foodInsecurePeoplePerStateData) {
    var tmpDict: { state: string; amt: number } = {
      state: "",
      amt: 0,
    };
    tmpDict["state"] = currState;
    tmpDict["amt"] = foodInsecurePeoplePerStateData[currState];
    returnVal.push(tmpDict);
  }
  return returnVal;
}
function getBanksData(banksData: IPantryArray[]) {
  var stateBanksData: { [key: string]: number } = {};
  var data: { [key: number]: { [keyy: string]: any } } = banksData;

  for (var idx in data) {
    var state = data[idx]["stateOrProvince"];
    if (stateBanksData[state] == undefined) {
      stateBanksData[state] = 1;
    } else {
      stateBanksData[state] += 1;
    }
  }

  var returnVal: VisArray[] = [];
  for (var currState in stateBanksData) {
    var tmpDict: { state: string; amt: number } = {
      state: "",
      amt: 0,
    };
    tmpDict.state = currState;
    tmpDict.amt = stateBanksData[currState];
    returnVal.push(tmpDict);
  }
  return returnVal;
}

function getRecipesData(recipeData: IRecipeArray[]) {
  var prepTimeToLikesData: { [key: string]: number } = {};
  var data: { [key: number]: { [keyy: string]: any } } = recipeData;
  var returnVal: { time: number; a_likes: number; meal: string }[] = [];

  for (var idx in data) {
    var likes = data[idx]["aggregateLikes"];
    var prepTime = data[idx]["readyInMinutes"];
    var meal = data[idx]["title"];
    var tmpDict: { time: number; a_likes: number; meal: string } = {
      time: 0,
      a_likes: 0,
      meal: "",
    };
    tmpDict.a_likes = likes;
    tmpDict.time = prepTime;
    tmpDict.meal = meal;
    returnVal.push(tmpDict);
  }
  returnVal = returnVal.sort((a, b) => a.time - b.time);
  return returnVal;
}

function VisualizationPageLanding() {
  const [recipeData, setRecipeData] = React.useState<IRecipeArray[]>([]);
  const [banksData, setBankData] = React.useState<IPantryArray[]>([]);
  const [statsData, setStatsData] = React.useState<IStatsArray[]>([]);
  const [loading, setLoading] = React.useState(true);

  useEffect(() => {
    const statsUrl = API_ENDPOINT + "stats/all";
    axios
      .get(statsUrl)
      .then(function (response) {
        console.log(response);
        setStatsData(response.data);

        // handle succes
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }, []);

  useEffect(() => {
    const recipeUrl = API_ENDPOINT + "recipe/all";
    axios
      .get(recipeUrl)
      .then(function (response) {
        console.log(response);
        setRecipeData(response.data);

        // handle succes
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }, []);
  useEffect(() => {
    const banksUrl = API_ENDPOINT + "banks/all";
    axios
      .get(banksUrl)
      .then(function (response) {
        console.log(response);
        setBankData(response.data);

        // handle succes
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }, []);

  const [statsVisData, setStatsVis] = useState<VisArray[]>([]);
  useEffect(() => {
    let vis = getStatsData(statsData);
    console.log(vis);
    setStatsVis(vis);
  }, [statsData]);

  const [banksVisData, setBanksVis] = useState<VisArray[]>([]);
  useEffect(() => {
    let vis = getBanksData(banksData);
    console.log(vis);
    setBanksVis(vis);
  }, [banksData]);

  const [recipeVisData, setRecipeVis] = useState<VisArray[]>([]);
  useEffect(() => {
    let vis = getRecipesData(recipeData);
    console.log(vis);
    setRecipeVis(vis);
    setLoading(false);
  }, [recipeData]);

  return (
    <div className="VisualizationBody">
      <Typography variant="h1" gutterBottom component="div">
        Our Visualizations
      </Typography>
      <Typography variant="h3" gutterBottom component="div">
        Food Banks Per State
      </Typography>
      {loading ? (
        console.log("loading")
      ) : (
        <div className="ChartWrapper">
          <ResponsiveContainer>
            <PieChart>
              <Pie
                data={banksVisData}
                dataKey="amt"
                nameKey="state"
                fill="#f9a826"
                label
              />
              <Tooltip />
            </PieChart>
          </ResponsiveContainer>
        </div>
      )}

      <Typography variant="h3" gutterBottom component="div">
        Food Insecure People Per State ( according to self reported data from
        Census )
      </Typography>

      {loading ? (
        console.log("loading")
      ) : (
        <div className="ChartWrapper">
          <ResponsiveContainer>
            <BarChart data={statsVisData}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="state" name="State" />
              <YAxis dataKey="amt" />
              <Tooltip />
              <Legend />
              <Bar
                dataKey="amt"
                fill="#f9a826"
                name="Number of Food Insecure People"
              />
            </BarChart>
          </ResponsiveContainer>
        </div>
      )}

      <Typography variant="h3" gutterBottom component="div">
        Comparing Recipe Likes to Prep Time
      </Typography>

      {loading ? (
        console.log("loading")
      ) : (
        <div className="ChartWrapper">
          <ResponsiveContainer>
            <ScatterChart margin={{ top: 20, right: 20, bottom: 10, left: 10 }}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="time" name="Prep Time" unit="min" />
              <YAxis dataKey="a_likes" name="Aggregate Likes" />
              <ZAxis dataKey="meal" name="Meal" />

              <Tooltip cursor={{ strokeDasharray: "3 3" }} />
              <Legend />
              <Scatter name="Recipe" data={recipeVisData} fill="#f9a826" />
            </ScatterChart>
          </ResponsiveContainer>
        </div>
      )}
    </div>
  );
}
export default VisualizationPageLanding;
