interface IRecipeVis {
  a_likes: number;
  time: number;
}

interface IStatVis {
  state: string;
  amt: number;
}

interface IBankVis {
  state: string;
  amt: number;
}

export default interface VisArray {
  [key: number]: IBankVis | IStatVis | IRecipeVis;
}
