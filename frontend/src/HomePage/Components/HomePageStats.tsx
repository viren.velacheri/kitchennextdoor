import React from "react";

import Button from "react-bootstrap/Button";
import { Container, Row, Col } from "react-bootstrap";
import "../Style/HomePageStats.css";
import { Link } from "react-router-dom";

const HomePageStats = () => {
  return (
    <div>
      <Container>
        <Row>
          <Col>
            <div className="stat1">
              <p className="BigNumber">1 in 8 Texans</p>
              experiences food insecurity
            </div>
          </Col>
          <Col>
            <div className="stat2">
              <p className="BigNumber">450,000,000</p>
              pounds of food wasted in Austin annually
            </div>
          </Col>
          <Col>
            <div className="stat3">
              <p className="BigNumber">Interested in learning more?</p>
              <Link to="/stats/view">
                <Button variant="secondary">
                  Find Out How Hunger is Affecting the US
                </Button>
              </Link>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default HomePageStats;
