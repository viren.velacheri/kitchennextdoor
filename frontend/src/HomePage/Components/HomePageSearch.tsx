import Form from "react-bootstrap/Form";
import React from "react";
import { useNavigate } from "react-router-dom";
import "../Style/HomePageSearch.css";

const HomePageSearch = () => {
  const navigate = useNavigate();
  function search(val: string) {
    navigate("/search/view/" + val);
  }
  return (
    <div className="SearchWrapper">
      <Form.Label className="label" htmlFor="inputSearch">Search</Form.Label>
      <Form.Control
        type="search"
        placeholder="Search here"
        id="inputSearch"
        aria-describedby="searchHelpBlock"
        onKeyPress={(event) => {
          if (event.key === "Enter") {
            search((event.target as HTMLTextAreaElement).value);
          }
        }}
      />
    </div>
  );
};

export default HomePageSearch;
