import React from "react";
import GooglePlacesAutocomplete from "react-google-places-autocomplete";
import Button from "react-bootstrap/Button";
import eating_together from "../Assets/eating_together.svg";
import { Container, Row, Col } from "react-bootstrap";
import "../Style/HomePagePantry.css";
import { Link } from "react-router-dom";

const HomePagePantry = () => {
  return (
    <div>
      <Container>
        <Row>
          <Col>
            <div className="DivLeft">
              <p>In Need of a Meal?</p>
              <Link to="/banks/view">
                <Button variant="secondary">Find a Pantry Near You</Button>
              </Link>
            </div>
          </Col>

          <Col>
            <img src={eating_together} className="PantryImage" alt="logo" />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default HomePagePantry;
