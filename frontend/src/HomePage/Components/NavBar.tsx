import { Navbar, Nav, Container, NavDropdown } from "react-bootstrap";
import React, { Component } from "react";
import "../Style/NavBar.css";
import kndLogo from "../Assets/kndLogo.png";

const NavigationBar = () => {
  return (
    <Navbar className="navbar" fixed="top" bg="dark" variant="dark">
      <Container>
        <img src={kndLogo} className="Logo" alt="logo" />
        <Navbar.Brand href="/home">KitchenNextDoor</Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link href="/about">About</Nav.Link>
          <Nav.Link href="/banks/view">Find a Pantry</Nav.Link>
          <Nav.Link href="/stats/view">Hunger Statistics</Nav.Link>
          <Nav.Link href="/recipes/view">Recipes</Nav.Link>
          <Nav.Link href="/visualizations/view"> Our Visualizations</Nav.Link>
          <Nav.Link href="/providervisualizations/view"> Provider Visualizations</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
};

export default NavigationBar;
