import React from "react";
import GooglePlacesAutocomplete from "react-google-places-autocomplete";
import Button from "react-bootstrap/Button";
import chef from "../Assets/chef.svg";
import { Container, Row, Col } from "react-bootstrap";
import "../Style/HomePageRecipes.css";
import { Link } from "react-router-dom";

const HomePageRecipes = () => {
  return (
    <div>
      <Container>
        <Row>
          <Col>
            <div className="DivLeft">
              <p>Want to Help Out?</p>
              <Link to="/recipes/view">
                <Button variant="secondary">
                  Learn how to Cook Meals to Donate
                </Button>
              </Link>
            </div>
          </Col>

          <Col>
            <img src={chef} className="ChefImage" alt="logo" />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default HomePageRecipes;
