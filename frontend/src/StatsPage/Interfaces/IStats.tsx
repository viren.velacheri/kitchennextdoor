export default interface IStats {
  id: number;
  county: string;
  state: string;
  snap: string;
  costPerMeal: string;
  foodInsecureChildren: string;
  foodInsecurityRateChildren: string;
  foodInsecurePeople: string;
  foodInsecurityRate: string;
  wentToFoodPantry: string;
  foodWorry: string;
  photo: string;
  lat: number;
  lng: number;
}
export default interface IStatsArray {
  [key: number]: IStats;
}

export const STATS_API = "http://localhost:5000/stats";
