import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import "../Style/StatsCard.css";

interface Props {
  img_src?: string;
  name?: string;
  attributes?: string;
  statsData?: any[];
  id?: number;
  state?: string;
  costPerMeal?: string;
  foodInsecurePeople?: string;
  foodInsecurityRate?: string;
  search?: string;
}

const Highlighted = ({ text = "", highlight = "" }) => {
  if (!highlight.trim()) {
    return <span>{text}</span>;
  }
  const regex = new RegExp(`(${highlight})`, "gi");
  const parts = text.split(regex);

  return (
    <span>
      {parts.filter(String).map((part, i) => {
        return regex.test(part) ? (
          <mark key={i}>{part}</mark>
        ) : (
          <span key={i}>{part}</span>
        );
      })}
    </span>
  );
};

const StatsCard = (props: Props) => {
  return (
    <Grid item xs={3}>
      <Card sx={{ minHeight: "25vh" }}>
        <CardActionArea component={Link} to={"/stats/view/" + props.id}>
          <CardMedia
            component="img"
            height="200px"
            width="auto"
            alt="city pic"
            src={props.img_src}
          />
          <CardContent sx={{ width: 1 }}>
            <Typography gutterBottom variant="h5" component="div">
              <Highlighted text={props.name} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              State: <Highlighted text={props.state} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Cost Per Meal:{" "}
              <Highlighted text={props.costPerMeal} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Number of Food Insecure People:{" "}
              <Highlighted
                text={props.foodInsecurePeople}
                highlight={props.search}
              />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Food Insecurity Rate:{" "}
              <Highlighted
                text={props.foodInsecurityRate}
                highlight={props.search}
              />
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
};

export default StatsCard;
