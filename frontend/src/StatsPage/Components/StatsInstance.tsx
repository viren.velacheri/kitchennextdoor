import React from "react";
import { Container, Row } from "react-bootstrap";
import "../Style/StatsInstance.css";
import axios from "axios";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { API_ENDPOINT } from "../../GlobalConsts";
import IStats from "../Interfaces/IStats";
import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import { ThemeProvider } from "@mui/material/styles";
import Theme from "../Interfaces/Theme";
import GoogleMapReact from "google-map-react";

function StatsInstance() {
  const [id, setId] = React.useState(0);
  const [didMount, setDidMount] = React.useState(false);

  // Get Id of recipe to call from db
  React.useEffect(() => {
    const my_url = window.location.href;
    var parts = my_url.split("/");
    var result = parts[parts.length - 1];
    const url_id = Number(result);
    console.log(url_id);
    setId(url_id);
    setDidMount(true);
  }, []);

  let initData: IStats = {
    id: 0,
    county: "",
    state: "",
    snap: "",
    costPerMeal: "",
    foodInsecureChildren: "",
    foodInsecurityRateChildren: "",
    foodInsecurePeople: "",
    foodInsecurityRate: "",
    wentToFoodPantry: "",
    foodWorry: "",
    photo: "",
    lat: 0,
    lng: 0,
  };

  // Get specific recipe from db
  const [stats, setStats] = React.useState<IStats>(initData);

  const [loading, setLoading] = React.useState(true);
  const local_url = "http://localhost:5000" + "/stats/" + id;
  const url = API_ENDPOINT + "stats/" + id;

  React.useEffect(() => {
    if (didMount) {
      console.log(url);
      axios
        .get(url)
        .then(function (response) {
          let statsData = response.data[0];
          setStats(statsData);
          setLoading(false);

          // handle succes
          console.log(statsData);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
  }, [id]);

  const [pantryId, setPantryId] = React.useState(1);

  React.useEffect(() => {
    var splitted = stats.county.split(" ", 2);
    const url_connect =
      API_ENDPOINT + "banks/" + stats.state + "/" + splitted[0];
    if (didMount) {
      axios
        .get(url_connect)
        .then(function (response) {
          // handle succes
          let bankData = response.data[0];
          setPantryId(bankData.id);
          console.log(bankData);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
  }, [stats]);

  const [recipeId, setRecipeId] = React.useState(1);
  React.useEffect(() => {
    const url_connect_rec = API_ENDPOINT + "recipe/" + stats.state;
    if (didMount) {
      axios
        .get(url_connect_rec)
        .then(function (response) {
          // handle succes
          let recData = response.data[0];
          setRecipeId(recData.id);
          setLoading(false);
          console.log(recData);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
  }, [stats]);

  return (
    <div className="StatsInstance">
      <p></p>
      {loading ? (
        console.log("Loading")
      ) : (
        <Box sx={{ width: "100%", maxWidth: "100%" }}>
          <Container>
            <Row>
              <Typography variant="h2" gutterBottom component="div">
                {stats.county}
              </Typography>
            </Row>

            <Typography variant="h4" gutterBottom component="div">
              State: {stats.state}
            </Typography>

            <Row>
              <div className="ImageWrapper">
                <img
                  className="CountyPic"
                  src={
                    "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1600&photo_reference=" +
                    stats.photo +
                    "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4"
                  }
                  alt="County Pic"
                ></img>
              </div>
            </Row>
            <Row>
              <div className="DividerWrapper">
                <Divider></Divider>
              </div>
            </Row>

            <Typography variant="h6" gutterBottom component="div">
              SNAP: {stats.snap}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Cost Per Meal: {stats.costPerMeal}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Number of Food Insecure Children: {stats.foodInsecureChildren}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Number of Food Insecure People: {stats.foodInsecurePeople}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Food Insecurity Rate: {stats.foodInsecurityRate}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Food Insecurity Rate for Children:{" "}
              {stats.foodInsecurityRateChildren}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Number That Went to Food Pantry: {stats.wentToFoodPantry}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Food Worry: {stats.foodWorry}
            </Typography>
            <div className="ButtonWrapper">
              <ThemeProvider theme={Theme}>
                <Button
                  variant="contained"
                  href={"/recipes/view/" + recipeId}
                  color="primary"
                  disableElevation
                >
                  Popular Food
                </Button>
              </ThemeProvider>

              <ThemeProvider theme={Theme}>
                <Button
                  variant="contained"
                  href={"/banks/view/" + pantryId}
                  color="primary"
                  disableElevation
                >
                  Local Food Pantry
                </Button>
              </ThemeProvider>
            </div>

            <Row>
              <div className="DividerWrapper">
                <Divider></Divider>
              </div>
            </Row>
            <Row>
              <div className="MapWrapper">
                <GoogleMapReact
                  bootstrapURLKeys={{
                    key: "AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4",
                    language: "en",
                  }}
                  defaultCenter={{ lat: stats.lat, lng: stats.lng }}
                  defaultZoom={10}
                ></GoogleMapReact>
              </div>
            </Row>
          </Container>
        </Box>
      )}
    </div>
  );
}

export default StatsInstance;
