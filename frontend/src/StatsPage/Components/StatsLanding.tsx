import React from "react";
import StatsCard from "./StatsCard";
import "../Style/StatsLanding.css";
import { Link, useLocation } from "react-router-dom";
import FilterBar from "../../Filters/FilterBar";
import {
  StatsFilterChoices,
  StatsFilterFields,
} from "../../StatsPage/Interfaces/FilterValues";
import Grid from "@mui/material/Grid";
import axios from "axios";
import { API_ENDPOINT } from "../../GlobalConsts";
import statsTestData from "./testData.json";
import Typography from "@mui/material/Typography";
import Pagination from "@mui/material/Pagination";
import IStatsArray from "../Interfaces/IStats";
import PaginationItem from "@mui/material/PaginationItem";

function StatsLanding() {
  // State Values to hold filter vals
  const [field1, setField1] = React.useState("");
  const [field2, setField2] = React.useState("");
  const [field3, setField3] = React.useState("");
  const [field4, setField4] = React.useState("");
  const [field5, setField5] = React.useState("");
  const [search, setSearch] = React.useState("");

  function eventHandler(data: string, id: number) {
    switch (id) {
      case 1:
        setField1(data);
        break;
      case 2:
        setField2(data);
        break;
      case 3:
        setField3(data);
        setNumPerPage(Number(data));
        break;
      case 4:
        setField4(data);
        break;
      case 5:
        setField5(data);
        break;
      case 6:
        console.log("SERACHing");
        setSearch(data);
        break;
      default:
        console.log("DEFAULT");
        break;
    }
  }

  // Listeners that wait for state vals to get updated
  React.useEffect(() => {
    console.log(field1);
  }, [field1]);

  React.useEffect(() => {
    console.log(field2);
  }, [field2]);

  React.useEffect(() => {
    console.log(field3);
  }, [field3]);

  React.useEffect(() => {
    console.log(field4);
  }, [field4]);

  React.useEffect(() => {
    console.log(field5);
  }, [field5]);

  React.useEffect(() => {
    console.log("SERACH");
    console.log(search);
  }, [search]);

  const filterChoices = StatsFilterChoices;
  const filterFields = StatsFilterFields;

  // Load in Recipe Data

  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const page = parseInt(query.get("page") || "1", 10);

  const url = API_ENDPOINT + "stats";
  const local_url = "http://localhost:5000" + "/stats";

  const [currPage, setCurrPage] = React.useState(page);

  console.log(page + "" + currPage);
  const changePage = (event: React.ChangeEvent<unknown>, page: number) => {
    setCurrPage(page);
  };

  React.useEffect(() => {
    setCurrPage(page);
  }, [page]);

  const [numPerPage, setNumPerPage] = React.useState(10);

  const [statsData, setStatsData] = React.useState<IStatsArray[]>([]);
  const [loading, setLoading] = React.useState(true);

  const [totalNumPages, setPages] = React.useState(0);

  React.useEffect(() => {
    let test_url = url;
    test_url += "?page=" + currPage;
    if (numPerPage == 0) {
      test_url += "&numPerPage=5";
    } else {
      test_url += "&numPerPage=" + numPerPage;
    }

    test_url += "&state=" + field1;
    test_url += "&sort=" + field2;
    test_url += "&search=" + search;
    console.log(test_url);

    axios
      .get(test_url)
      .then(function (response) {
        // handle success
        console.log(currPage);
        setStatsData(response.data.data);
        setPages(response.data.pages);
        setLoading(false);
        console.log(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }, [currPage, numPerPage, field1, field2, field3, field4, field5, search]);

  return (
    <div className="StatsBody">
      <Typography variant="h2" gutterBottom component="div">
        Counties
      </Typography>

      <Typography variant="h5" gutterBottom component="div">
        Learn more about how hunger is impacting counties throughout the US
      </Typography>

      <div className="FilterWrapper">
        <FilterBar
          onChange={eventHandler}
          fields={filterFields}
          fieldChoices={filterChoices}
        />
      </div>
      <div className="CardsWrapper">
        {loading ? (
          console.log("loading")
        ) : (
          <Grid container spacing={2} alignItems="stretch">
            {statsData.map(function (stat) {
              let img_url =
                "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1600&photo_reference=" +
                stat.photo +
                "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4";
              return (
                <StatsCard
                  name={stat.county}
                  id={stat.id}
                  img_src={img_url}
                  state={stat.state}
                  costPerMeal={stat.costPerMeal}
                  foodInsecurePeople={stat.foodInsecurePeople}
                  foodInsecurityRate={stat.foodInsecurityRate}
                  search={search}
                />
              );
            })}
          </Grid>
        )}

        {loading ? (
          console.log("loading")
        ) : (
          <div className="PaginationWrapper">
            <Pagination
              page={page}
              count={totalNumPages}
              onChange={changePage}
              renderItem={(item) => (
                <PaginationItem
                  component={Link}
                  to={`/stats/view${
                    item.page === 1 ? "" : `?page=${item.page}`
                  }`}
                  {...item}
                />
              )}
            />
          </div>
        )}
      </div>
    </div>
  );
}
export default StatsLanding;
