import React from "react";
import {
  HomePagePantry,
  HomePageRecipes,
  HomePageStats,
  HomePageSearch,
} from "../HomePage/Components";
import ReactPlayer from "react-player";

const Home = () => {
  return (
    <main>
      <div>
        <HomePageSearch />
      </div>
      <div>
        <HomePagePantry />
        <HomePageStats />
        <HomePageRecipes />
      </div>
      <div>
        <h3> Team Presentation</h3>
        <ReactPlayer url='https://www.youtube.com/watch?v=9sn-_wVMdH4' />
      </div>
    </main>
  );
};

export default Home;
