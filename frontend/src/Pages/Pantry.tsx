import React from "react";
import "reset-css";
import { NavigationBar } from "../HomePage/Components";
// import FoodPantryCard from './FoodPantryCard';
import { Container, Row, Col } from "react-bootstrap";
import image1 from "../PantryPage/Assets/CTFB.jpg";
import image2 from "../PantryPage/Assets/HFB.jpg";
import image3 from "../PantryPage/Assets/NTFB.jpg";
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Pagination } from "react-bootstrap";

const Pantry = () => {
  return (
    <div className="Div">
      <header>
        <NavigationBar />
      </header>

      <div className="intro">
        <p className="bigText">
          <b> Find the pantries you are interested in!</b>
        </p>
        <Container>
          <Row>
            <Col>
              <Card>
                <Card.Img variant="top" src={image1} />
                <Card.Body>
                  <Card.Title>Central Texas Food Bank</Card.Title>
                  <Card.Text>City - Austin</Card.Text>
                  <Link to="/pantries/view/1">
                    <Button variant="primary">Click for more details</Button>
                  </Link>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card>
                <Card.Img variant="top" src={image2} />
                <Card.Body>
                  <Card.Title>Houston Food Bank</Card.Title>
                  <Card.Text>City - Houston</Card.Text>
                  <Link to="/pantries/view/2">
                    <Button variant="primary">Click for more details</Button>
                  </Link>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card>
                <Card.Img variant="top" src={image3} />
                <Card.Body>
                  <Card.Title>North Texas Food Bank</Card.Title>
                  <Card.Text>City - Dallas</Card.Text>
                  <Link to="/pantries/view/3">
                    <Button variant="primary">Click for more details</Button>
                  </Link>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>

      <footer>
        <Container>
          <Row>
            <Col></Col>
            <Col></Col>
            <Col>
              <span>Results 1 - 3</span>
              <Pagination>
                <Pagination.First />
                <Pagination.Prev />
                <Pagination.Item active>{1}</Pagination.Item>
                <Pagination.Next />
                <Pagination.Last />
              </Pagination>
            </Col>
          </Row>
        </Container>
      </footer>
    </div>
  );
};

export default Pantry;
