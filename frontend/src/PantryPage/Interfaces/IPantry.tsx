export default interface IPantry {
  id: number;
  name: string;
  tagline: string;
  mission: string;
  websiteURL: string;
  lat: number;
  lng: number;
  photo: string;
  stateOrProvince: string;
  county: string;
  mailing_address: {country: string, stateOrProvince: string, city: string, 
  postalCode: string, streetAddress1: string, streetAddress2: string};
}

export default interface IPantryArray {
  [key: number]: IPantry;
}

export const PANTRY_API = "http://localhost:5000/banks";
