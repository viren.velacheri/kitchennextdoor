import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import "../Style/PantryCard.css";

interface Props {
  img_src?: string;
  name?: string;
  attributes?: string;
  pantryData?: any[];
  state?: string;
  tagline?: string;
  mission?: string;
  websiteURL?: string;
  id?: number;
  county?: string;
  search?: string;
}

const Highlighted = ({ text = "", highlight = "" }) => {
  if (!highlight.trim()) {
    return <span>{text}</span>;
  }
  const regex = new RegExp(`(${highlight})`, "gi");
  const parts = text.split(regex);

  return (
    <span>
      {parts.filter(String).map((part, i) => {
        return regex.test(part) ? (
          <mark key={i}>{part}</mark>
        ) : (
          <span key={i}>{part}</span>
        );
      })}
    </span>
  );
};

const PantryCard = (props: Props) => {
  return (
    <Grid item xs={3}>
      <Card sx={{ minHeight: "25vh" }}>
        <CardActionArea component={Link} to={"/banks/view/" + props.id}>
          <CardMedia
            component="img"
            height="200px"
            alt="bank pic"
            src={props.img_src}
          />
          <CardContent sx={{ width: 1 }}>
            <Typography gutterBottom variant="h5" component="div">
              <Highlighted text={props.name} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              State: <Highlighted text={props.state} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Tagline:{" "}
              <Highlighted text={props.tagline} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Mission:{" "}
              <Highlighted text={props.mission} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Website:{" "}
              <Highlighted text={props.websiteURL} highlight={props.search} />
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
};

export default PantryCard;
