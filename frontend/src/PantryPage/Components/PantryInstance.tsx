import React from "react";
import { Container, Row } from "react-bootstrap";
import "../Style/PantryInstance.css";
import axios from "axios";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { API_ENDPOINT } from "../../GlobalConsts";
import IPantry from "../Interfaces/IPantry";
import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import { ThemeProvider } from "@mui/material/styles";
import Theme from "../Interfaces/Theme";
import GoogleMapReact from "google-map-react";

function PantryInstance() {
  const [id, setId] = React.useState(0);
  const [didMount, setDidMount] = React.useState(false);

  // Get Id of recipe to call from db
  React.useEffect(() => {
    const my_url = window.location.href;
    var parts = my_url.split("/");
    var result = parts[parts.length - 1];
    const url_id = Number(result);
    console.log(url_id);
    setId(url_id);
    setDidMount(true);
  }, []);

  let initData: IPantry = {
    id: 0,
    name: "",
    tagline: "",
    mission: "",
    websiteURL: "",
    lat: 0,
    lng: 0,
    photo: "",
    stateOrProvince: "",
    county: "",
    mailing_address: {
      country: "",
      stateOrProvince: "",
      city: "",
      postalCode: "",
      streetAddress1: "",
      streetAddress2: "",
    },
  };

  // Get specific recipe from db
  const [pantry, setPantry] = React.useState<IPantry>(initData);

  const [loading, setLoading] = React.useState(true);
  const local_url = "http://localhost:5000" + "/banks/" + id;

  React.useEffect(() => {
    if (didMount) {
      const url = API_ENDPOINT + "banks/" + id;

      console.log(url);
      axios
        .get(url)
        .then(function (response) {
          let pantryData = response.data[0];
          setPantry(pantryData);

          // handle succes
          console.log(pantryData);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
  }, [id]);

  const [county, setCounty] = React.useState(1);

  React.useEffect(() => {
    if (didMount) {
      const connect_url =
        API_ENDPOINT + "stats/" + pantry.stateOrProvince + "/" + pantry.county;
      const local_url_connect =
        "http://localhost:5000/stats/" +
        pantry.stateOrProvince +
        "/" +
        pantry.county;

      axios
        .get(connect_url)
        .then(function (response) {
          let countyData = response.data[0];
          setCounty(countyData.id);

          // handle succes
          console.log(countyData);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
  }, [pantry]);

  const [recipeId, setRecipeId] = React.useState(1);
  React.useEffect(() => {
    const url_connect_rec = API_ENDPOINT + "recipe/" + pantry.stateOrProvince;
    if (didMount) {
      axios
        .get(url_connect_rec)
        .then(function (response) {
          // handle succes
          let recData = response.data[0];
          setRecipeId(recData.id);
          setLoading(false);
          console.log(recData);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
  }, [pantry]);

  pantry.mailing_address.country = "USA";
  pantry.mailing_address.streetAddress2 = "None";

  return (
    <div className="PantryInstance">
      <p></p>
      {loading ? (
        console.log("Loading")
      ) : (
        <Box sx={{ width: "100%", maxWidth: "100%" }}>
          <Container>
            <Row>
              <Typography variant="h2" gutterBottom component="div">
                {pantry.name}
              </Typography>
            </Row>

            <Typography variant="h6" gutterBottom component="div">
              State: {pantry.stateOrProvince}
            </Typography>

            <Typography variant="h6" gutterBottom component="div">
              Mission: {pantry.mission}
            </Typography>

            <Row>
              {/* What to do for Pantry Map picture */}
              <div className="ImageWrapper">
                <img
                  className="CountyPic"
                  src={
                    "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1600&photo_reference=" +
                    pantry.photo +
                    "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4"
                  }
                  alt="Pantry Pic"
                ></img>
              </div>
            </Row>
            <Row>
              <div className="DividerWrapper">
                <Divider></Divider>
              </div>
            </Row>

            <Typography variant="h6" gutterBottom component="div">
              Tagline: {pantry.tagline}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Website URL: {pantry.websiteURL}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              City: {pantry.mailing_address.city}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Postal Code: {pantry.mailing_address.postalCode}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Street Address 1: {pantry.mailing_address.streetAddress1}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Street Address 2: {pantry.mailing_address.streetAddress2}
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              Country: {pantry.mailing_address.country}
            </Typography>

            <div className="ButtonWrapper">
              <ThemeProvider theme={Theme}>
                <Button
                  variant="contained"
                  href={"/recipes/view/" + recipeId}
                  color="primary"
                  disableElevation
                >
                  Popular Food
                </Button>
              </ThemeProvider>

              <ThemeProvider theme={Theme}>
                <Button
                  variant="contained"
                  href={"/stats/view/" + county}
                  color="primary"
                  disableElevation
                >
                  Local Statistics
                </Button>
              </ThemeProvider>
            </div>

            <Row>
              <div className="DividerWrapper">
                <Divider></Divider>
              </div>
            </Row>
            <Row>
              <div className="MapWrapper">
                <GoogleMapReact
                  bootstrapURLKeys={{
                    key: "AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4",
                    language: "en",
                  }}
                  defaultCenter={{ lat: pantry.lat, lng: pantry.lng }}
                  defaultZoom={10}
                ></GoogleMapReact>
              </div>
            </Row>
          </Container>
        </Box>
      )}
    </div>
  );
}

export default PantryInstance;
