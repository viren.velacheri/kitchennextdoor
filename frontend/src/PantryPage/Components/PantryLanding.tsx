import React from "react";
import PantryCard from "./PantryCard";
import "../Style/PantryLanding.css";
import FilterBar from "../../Filters/FilterBar";
import {
  PantryFilterChoices,
  PantryFilterFields,
} from "../../PantryPage/Interfaces/FilterValues";
import Grid from "@mui/material/Grid";
import axios from "axios";
import { API_ENDPOINT } from "../../GlobalConsts";
import Pagination from "@mui/material/Pagination";
import IPantryArray from "../Interfaces/IPantry";
import Typography from "@mui/material/Typography";
import PaginationItem from "@mui/material/PaginationItem";
import { Link, useLocation } from "react-router-dom";

function PantryLanding() {
  // State Values to hold filter vals
  const [field1, setField1] = React.useState("");
  const [field2, setField2] = React.useState("");
  const [field3, setField3] = React.useState("");
  const [field4, setField4] = React.useState("");
  const [field5, setField5] = React.useState("");
  const [search, setSearch] = React.useState("");

  function eventHandler(data: string, id: number) {
    switch (id) {
      case 1:
        setField1(data);
        break;
      case 2:
        setField2(data);
        break;
      case 3:
        setField3(data);
        setNumPerPage(Number(data));
        break;
      case 4:
        setField4(data);
        break;
      case 5:
        setField5(data);
        break;

      case 6:
        setSearch(data);
        break;
      default:
        console.log("DEFAULT");
        break;
    }
  }

  const [numPerPage, setNumPerPage] = React.useState(10);

  // Listeners that wait for state vals to get updated
  React.useEffect(() => {
    console.log(field1);
  }, [field1]);

  React.useEffect(() => {
    console.log(field2);
  }, [field2]);

  React.useEffect(() => {
    console.log(field3);
  }, [field3]);

  React.useEffect(() => {
    console.log(field4);
  }, [field4]);

  React.useEffect(() => {
    console.log(field5);
  }, [field5]);
  React.useEffect(() => {
    console.log(search);
  }, [search]);

  const filterChoices = PantryFilterChoices;
  const filterFields = PantryFilterFields;

  // Load in Recipe Data

  const [currPage, setCurrPage] = React.useState(1);
  const url = API_ENDPOINT + "banks";
  const local_url = "http://localhost:5000" + "/banks";
  const changePage = (event: React.ChangeEvent<unknown>, page: number) => {
    setCurrPage(page);
  };

  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const page = parseInt(query.get("page") || "1", 10);

  React.useEffect(() => {
    setCurrPage(page);
  }, [page]);

  const [pantryData, setPantryData] = React.useState<IPantryArray[]>([]);
  const [loading, setLoading] = React.useState(true);

  const [totalNumPages, setPages] = React.useState(0);

  console.log("Trying axios");

  React.useEffect(() => {
    let test_url = url;
    test_url += "?page=" + currPage;
    if (numPerPage == 0) {
      test_url += "&numPerPage=5";
    } else {
      test_url += "&numPerPage=" + numPerPage;
    }
    test_url += "&state=" + field1;
    test_url += "&sort=" + field2;
    test_url += "&search=" + search;
    axios
      .get(test_url)
      .then(function (response) {
        console.log(response.data);
        setPantryData(response.data.data);
        setPages(response.data.pages);
        setLoading(false);
        // handle success
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }, [currPage, numPerPage, field1, field2, field3, field4, field5, search]);

  return (
    <div className="PantryBody">
      <Typography variant="h2" gutterBottom component="div">
        Pantries
      </Typography>

      <Typography variant="h5" gutterBottom component="div">
        Find the pantries closest to you!
      </Typography>

      <div className="FilterWrapper">
        <FilterBar
          onChange={eventHandler}
          fields={filterFields}
          fieldChoices={filterChoices}
        />
      </div>
      <div className="CardsWrapper">
        {loading ? (
          console.log("loading")
        ) : (
          <Grid container spacing={2} alignItems="stretch">
            {pantryData.map(function (pantry) {
              let img_url =
                "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1600&photo_reference=" +
                pantry.photo +
                "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4";
              return (
                <PantryCard
                  name={pantry.name}
                  id={pantry.id}
                  img_src={img_url}
                  state={pantry.stateOrProvince}
                  tagline={pantry.tagline}
                  mission={pantry.mission}
                  websiteURL={pantry.websiteURL}
                  search={search}
                />
              );
            })}
          </Grid>
        )}

        {loading ? (
          console.log("loading")
        ) : (
          <div className="PaginationWrapper">
            <Pagination
              page={page}
              count={totalNumPages}
              onChange={changePage}
              renderItem={(item) => (
                <PaginationItem
                  component={Link}
                  to={`/banks/view/?page=${item.page}`}
                  {...item}
                />
              )}
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default PantryLanding;
