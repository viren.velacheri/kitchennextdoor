import "./App.css";
import React from "react";

import "reset-css";
import { NavigationBar } from "./HomePage/Components";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Pantry from "./Pages/Pantry";
import AboutPageLanding from "./AboutPage/Components/AboutPageLanding";
import Home from "./Pages/Home";
import { RecipesLanding } from "./RecipesPage/Components";
import RecipeInstance from "./RecipesPage/Components/RecipeInstance";
import { PantryLanding } from "./PantryPage/Components";
import PantryInstance from "./PantryPage/Components/PantryInstance";
import { StatsLanding } from "./StatsPage/Components";
import StatsInstance from "./StatsPage/Components/StatsInstance";
import { SearchLanding } from "./SearchPage/Components";
import { VisualizationPageLanding } from "./VisualizationPage/Components";
import ProviderVisualizationPageLanding from "./VisualizationPage/Components/ProviderVisualizationPageLanding";
function App() {
  return (
    <BrowserRouter>
      <div className="Div">
        <header>
          <NavigationBar />
        </header>
        <Routes>
          <Route path="" element={<Home />} />
          <Route path="/about" element={<AboutPageLanding />} />
          <Route path="/pantries/view" element={<Pantry />} />
          <Route path="/stats/view" element={<StatsLanding />} />
          <Route path="/recipes/view" element={<RecipesLanding />} />
          <Route path="/home" element={<Home />} />
          <Route path="/banks/view" element={<PantryLanding />} />
          <Route path="/banks/view/:id" element={<PantryInstance />} />
          <Route path="/recipes/view/:id" element={<RecipeInstance />} />
          <Route path="/stats/view/:id" element={<StatsInstance />} />
          <Route
            path="/visualizations/view"
            element={<VisualizationPageLanding />}
          />
          <Route
            path="/providervisualizations/view"
            element={<ProviderVisualizationPageLanding />}
          />

          <Route path="/search/view/:search" element={<SearchLanding />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
