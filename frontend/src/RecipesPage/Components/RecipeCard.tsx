import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import IRecipe from "../Interfaces/IRecipe";
import "../Style/RecipeCard.css";

interface Props {
  img_src?: string;
  name?: string;
  attributes?: string;
  recipeData?: IRecipe;
  id?: number;
  mealPrepTime?: number;
  calories?: number;
  likes?: number;
  healthScore?: number;
  search?: string;
}

const Highlighted = ({ text = "", highlight = "" }) => {
  if (!highlight.trim()) {
    return <span>{text}</span>;
  }
  const regex = new RegExp(`(${highlight})`, "gi");
  const parts = text.split(regex);

  return (
    <span>
      {parts.filter(String).map((part, i) => {
        return regex.test(part) ? (
          <mark key={i}>{part}</mark>
        ) : (
          <span key={i}>{part}</span>
        );
      })}
    </span>
  );
};

const RecipeCard = (props: Props) => {
  let real_prep_time = props.mealPrepTime!.toString();
  let real_calories = props.calories!.toString();
  let real_likes = props.likes!.toString();
  let real_healthscore = props.healthScore!.toString();
  return (
    <Grid item xs={3}>
      <Card sx={{ minHeight: "25vh" }}>
        <CardActionArea component={Link} to={"/recipes/view/" + props.id}>
          <CardMedia
            component="img"
            height="200px"
            alt="food pic"
            src={props.img_src}
          />
          <CardContent sx={{ width: 1 }}>
            <Typography gutterBottom variant="h5" component="div">
              <Highlighted text={props.name} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Meal Prep Time in Minutes:{" "}
              <Highlighted text={real_prep_time} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Calories:{" "}
              <Highlighted text={real_calories} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Likes: <Highlighted text={real_likes} highlight={props.search} />
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Health Score:{" "}
              <Highlighted text={real_healthscore} highlight={props.search} />
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
};

export default RecipeCard;
