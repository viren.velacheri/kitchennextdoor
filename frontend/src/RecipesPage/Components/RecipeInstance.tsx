import React from "react";
import { Container, Row } from "react-bootstrap";
import "../Style/RecipeInstance.css";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import axios from "axios";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { API_ENDPOINT } from "../../GlobalConsts";
import IRecipe from "../Interfaces/IRecipe";
import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import RestaurantIcon from "@mui/icons-material/Restaurant";
import { ThemeProvider } from "@mui/material/styles";
import Theme from "../Interfaces/Theme";

interface Step {
  number: number;
  step: string;
}

function RecipeInstance() {
  const [id, setId] = React.useState(0);
  const [didMount, setDidMount] = React.useState(false);

  // Get Id of recipe to call from db
  React.useEffect(() => {
    const my_url = window.location.href;
    var parts = my_url.split("/");
    var result = parts[parts.length - 1];
    const url_id = Number(result);
    console.log(url_id);
    setId(url_id);
    setDidMount(true);
  }, []);
  let initData: IRecipe = {
    aggregateLikes: 0,
    analyzedInstructions: [],
    id: -1,
    healthScore: -1,
    type: "",
    dishTypes: [],
    ingredients: [],
    diets: [],
    cuisines: [],
    readyInMinutes: -1,
    calories: -1,
    pricePerServing: -1,
    summary: "",
    title: "",
    sourceUrl: "",
    image: "",
    equipmentPics: [],
  };

  // Get specific recipe from db
  const [recipe, setRecipe] = React.useState<IRecipe>(initData);
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    if (didMount) {
      const local_url = "http://localhost:5000/recipe/" + id;
      const url = API_ENDPOINT + "recipe/" + id;

      console.log(url);
      axios
        .get(url)
        .then(function (response) {
          let recipeData = response.data[0];

          console.log(response);
          setRecipe(recipeData);
          setLoading(false);

          // handle succes
          console.log(recipeData);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
  }, [id]);
  const [county, setCounty] = React.useState(1);

  const possibleCus = [
    "african",
    "american",
    "cajun",
    "caribbean",
    "chinese",
    "eastern european",
    "european",
    "french",
    "greek",
    "indian",
    "irish",
    "italian",
    "japanese",
    "jewish",
    "korean",
    "latin american",
    "mediterranean",
    "mexican",
    "middle eastern",
    "southern",
    "thai",
    "vietnamese",
  ];

  React.useEffect(() => {
    let cuisine = "";

    recipe.cuisines.forEach((cuisine1) => {
      if (possibleCus.includes(cuisine1)) {
        cuisine = cuisine1;
      }
    });
    const url_connect = API_ENDPOINT + "stats/recipe/" + cuisine;
    if (didMount) {
      axios
        .get(url_connect)
        .then(function (response) {
          // handle succes
          let countyData = response.data[0];
          setCounty(countyData.id);
          console.log(countyData);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
  }, [recipe]);

  const [bank, setBank] = React.useState(1);
  React.useEffect(() => {
    let cuisine = "";

    recipe.cuisines.forEach((cuisine1) => {
      if (possibleCus.includes(cuisine1)) {
        cuisine = cuisine1;
      }
    });
    const url_connect = API_ENDPOINT + "banks/recipe/" + cuisine;
    if (didMount) {
      axios
        .get(url_connect)
        .then(function (response) {
          // handle succes
          let bankData = response.data[0];
          setBank(bankData.id);
          setLoading(false);
          console.log(bankData);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
  }, [recipe]);

  return (
    <div className="RecipeInstance">
      <p></p>
      {loading ? (
        console.log("Loading")
      ) : (
        <Box sx={{ width: "100%", maxWidth: "100%" }}>
          <Container>
            <Row>
              <Typography variant="h2" gutterBottom component="div">
                {recipe.title}
              </Typography>
            </Row>

            <div className="SourceWrapper">
              <ThemeProvider theme={Theme}>
                <Button
                  variant="contained"
                  href={recipe.sourceUrl}
                  color="primary"
                  disableElevation
                >
                  Source
                </Button>
              </ThemeProvider>
            </div>

            <Typography variant="subtitle1" gutterBottom component="div">
              Likes: {recipe.aggregateLikes}
              <ThumbUpIcon />
            </Typography>

            <Row>
              <div className="ImageWrapper">
                <img
                  className="FoodPic"
                  src={recipe.image}
                  alt="Food Pic"
                ></img>
              </div>
            </Row>
            <Row>
              <div className="DividerWrapper">
                <Divider></Divider>
              </div>
            </Row>

            <div className="SummaryWrapper">
              <Typography variant="subtitle1" gutterBottom component="div">
                {recipe.summary}
              </Typography>
            </div>

            <div className="RecipeFacts">
              <Typography variant="h5" gutterBottom component="div">
                Recipe Facts
                <RestaurantIcon />
              </Typography>

              <Typography variant="subtitle1" gutterBottom component="div">
                Cook time: {recipe.readyInMinutes} minutes
              </Typography>
              <Typography variant="subtitle1" gutterBottom component="div">
                Health Score: {recipe.healthScore}
              </Typography>
              <Typography variant="subtitle1" gutterBottom component="div">
                Calories: {recipe.pricePerServing}
              </Typography>
            </div>

            <Typography variant="h4" gutterBottom component="div">
              Equipment Needed
            </Typography>

            <div className="EquipmentImageWrapper">
              {recipe.equipmentPics.map((item) => (
                <img
                  src={"https://spoonacular.com/cdn/equipment_100x100/" + item}
                  alt="equipment"
                />
              ))}
            </div>

            <div id="ingredients">
              <Typography variant="h4" gutterBottom component="div">
                Ingredients
              </Typography>

              <List>
                <ListItem sx={{ display: "list-item" }}>
                  {recipe.ingredients.map(function (ingredient) {
                    return (
                      <ListItemText
                        primary={
                          ingredient.amount +
                          " " +
                          ingredient.unit +
                          " " +
                          ingredient.name
                        }
                      />
                    );
                  })}
                </ListItem>
              </List>
            </div>

            <Typography variant="h4" gutterBottom component="div">
              Directions
            </Typography>

            <List>
              <ListItem sx={{ display: "list-item" }}>
                {recipe.analyzedInstructions[0].steps.map(function (
                  step: Step
                ) {
                  return (
                    <ListItemText primary={step.number + " " + step.step} />
                  );
                })}
              </ListItem>
            </List>

            <div className="ButtonWrapper">
              <ThemeProvider theme={Theme}>
                <Button
                  variant="contained"
                  href={"/stats/view/" + county}
                  color="primary"
                  disableElevation
                >
                  This Food is Popular In....
                </Button>
              </ThemeProvider>

              <ThemeProvider theme={Theme}>
                <Button
                  variant="contained"
                  href={"/banks/view/" + bank}
                  color="primary"
                  disableElevation
                >
                  Food Bank That Would Like This
                </Button>
              </ThemeProvider>
            </div>
          </Container>
        </Box>
      )}
    </div>
  );
}

export default RecipeInstance;
