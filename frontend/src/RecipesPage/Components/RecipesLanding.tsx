import React from "react";
import RecipeCard from "./RecipeCard";
import "../Style/RecipesLanding.css";
import FilterBar from "../../Filters/FilterBar";
import {
  RecipeFilterChoices,
  RecipeFilterFields,
} from "../Interfaces/FilterValues";
import Grid from "@mui/material/Grid";
import axios from "axios";
import { API_ENDPOINT } from "../../GlobalConsts";
import Pagination from "@mui/material/Pagination";
import IRecipeArray from "../Interfaces/IRecipe";
import Typography from "@mui/material/Typography";
import { Link, useLocation } from "react-router-dom";
import PaginationItem from "@mui/material/PaginationItem";
function RecipesLanding() {
  // State Values to hold filter vals
  const [field1, setField1] = React.useState("");
  const [field2, setField2] = React.useState("");
  const [field3, setField3] = React.useState("");
  const [field4, setField4] = React.useState("");
  const [field5, setField5] = React.useState("");
  const [search, setSearch] = React.useState("");

  // Used to get filter vals from child component
  function eventHandler(data: string, id: number) {
    switch (id) {
      case 1:
        setField1(data);
        break;
      case 2:
        setField2(data);
        break;
      case 3:
        setField3(data);
        break;
      case 4:
        setField4(data);
        break;
      case 5:
        setField5(data);
        setNumPerPage(Number(data));
        break;
      case 6:
        setSearch(data);
        break;
      default:
        console.log("DEFAULT");
        break;
    }
  }

  // Listeners that wait for state vals to get updated
  React.useEffect(() => {
    console.log(field1);
  }, [field1]);

  React.useEffect(() => {
    console.log(field2);
  }, [field2]);

  React.useEffect(() => {
    console.log(field3);
  }, [field3]);

  React.useEffect(() => {
    console.log(field4);
  }, [field4]);

  React.useEffect(() => {
    console.log(field5);
  }, [field5]);

  React.useEffect(() => {
    console.log(search);
  }, [search]);

  const filterChoices = RecipeFilterChoices;
  const filterFields = RecipeFilterFields;

  // Pagination
  const [currPage, setCurrPage] = React.useState(1);
  const changePage = (event: React.ChangeEvent<unknown>, page: number) => {
    setCurrPage(page);
  };

  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const page = parseInt(query.get("page") || "1", 10);

  React.useEffect(() => {
    setCurrPage(page);
  }, [page]);

  // Log when page changes
  React.useEffect(() => {
    console.log(currPage);
  }, [currPage]);

  const [numPerPage, setNumPerPage] = React.useState(10);

  const [recipeData, setRecipeData] = React.useState<IRecipeArray[]>([]);
  const [loading, setLoading] = React.useState(true);

  console.log("Trying axios");
  const url = API_ENDPOINT + "recipe";
  const local_url = "http://localhost:5000/recipe";

  const [totalNumPages, setPages] = React.useState(0);

  // Call to backend to get data, make call when page changes, num per page changes, or when any filter is selected
  React.useEffect(() => {
    let test_url = url;
    test_url += "?page=" + currPage;
    if (numPerPage == 0) {
      test_url += "&numPerPage=5";
    } else {
      test_url += "&numPerPage=" + numPerPage;
    }
    test_url += "&diet=" + field1;
    test_url += "&dishType=" + field2;
    test_url += "&cuisine=" + field3;
    test_url += "&sort=" + field4;
    test_url += "&search=" + search;
    console.log(test_url);
    axios
      .get(test_url)
      .then(function (response) {
        console.log(response.data);
        setRecipeData(response.data.data);
        setPages(response.data.pages);
        setLoading(false);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }, [currPage, numPerPage, field1, field2, field3, field4, field5, search]);

  return (
    <div className="RecipeBody">
      <Typography variant="h2" gutterBottom component="div">
        Recipes
      </Typography>

      <Typography variant="h5" gutterBottom component="div">
        Find recipes to prepare and donate
      </Typography>

      <div className="FilterWrapper">
        <FilterBar
          onChange={eventHandler}
          fields={filterFields}
          fieldChoices={filterChoices}
        />
      </div>
      <div className="CardsWrapper">
        {loading ? (
          console.log("loading")
        ) : (
          <Grid container spacing={2} alignItems="stretch">
            {recipeData.map(function (recipe) {
              return (
                <RecipeCard
                  name={recipe.title}
                  id={recipe.id}
                  img_src={recipe.image}
                  mealPrepTime={recipe.readyInMinutes}
                  likes={recipe.aggregateLikes}
                  calories={recipe.pricePerServing}
                  healthScore={recipe.healthScore}
                  search={search}
                />
              );
            })}
          </Grid>
        )}

        {loading ? (
          console.log("loading")
        ) : (
          <div className="PaginationWrapper">
            <Pagination
              page={page}
              count={totalNumPages}
              onChange={changePage}
              renderItem={(item) => (
                <PaginationItem
                  component={Link}
                  to={`/recipes/view${
                    item.page === 1 ? "" : `?page=${item.page}`
                  }`}
                  {...item}
                />
              )}
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default RecipesLanding;
