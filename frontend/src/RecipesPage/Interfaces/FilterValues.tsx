// Filter fields to choose from
// name: Name that is displayed on dropdown menu
// value: value used to actually filter items
const Diets = [
  { value: "vegetarian", name: "Vegetarian" },
  { value: "vegan", name: "Vegan" },
  { value: "paleo", name: "Paleo" },
  { value: "gluten free", name: "Gluten Free" },
  { value: "ketogenic", name: "Ketogenic" },
  { value: "pescetarian", name: "Pescetarian" },
];

const MealType = [
  { value: "main course", name: "Main Course" },
  { value: "side dish", name: "Side Dish" },
  { value: "dessert", name: "Dessert" },
  { value: "appetizer", name: "Appetizer" },
  { value: "salad", name: "Salad" },
  { value: "bread", name: "Bread" },
  { value: "breakfast", name: "Breakfast" },
  { value: "soup", name: "Soup" },
  { value: "beverage", name: "Beverage" },
  { value: "sauce", name: "Sauce" },
  { value: "marinade", name: "Marinade" },
  { value: "fingerfood", name: "Fingerfood" },
  { value: "drink", name: "Drink" },
  { value: "snack", name: "Snack" },
];

const Cuisine = [
  { name: "African", value: "african" },
  { name: "American", value: "american" },
  { name: "Cajun", value: "cajun" },
  { name: "Caribbean", value: "caribbean" },
  { name: "Chinese", value: "chinese" },
  { name: "Eastern European", value: "eastern european" },
  { name: "European", value: "european" },
  { name: "French", value: "french" },
  { name: "Greek", value: "greek" },
  { name: "Indian", value: "indian" },
  { name: "Irish", value: "irish" },
  { name: "Italian", value: "italian" },
  { name: "Japanese", value: "japanese" },
  { name: "Jewish", value: "jewish" },
  { name: "Korean", value: "korean" },
  { name: "Latin American", value: "latin american" },
  { name: "Mediterranean", value: "mediterranean" },
  { name: "Mexican", value: "mexican" },
  { name: "Middle Eastern", value: "middle eastern" },
  { name: "Southern", value: "southern" },
  { name: "Spanish", value: "spanish" },
  { name: "Thai", value: "thai" },
  { name: "Vietnamese", value: "vietnamese" },
];

const Sort = [
  { name: "Likes", value: "aggregateLikes" },
  { name: "Health Score", value: "healthScore" },
];

const NumPerPage = [
  { name: "5", value: "5" },
  { name: "10", value: "10" },
  { name: "20", value: "20" },
];
export const RecipeFilterChoices = [Diets, MealType, Cuisine, Sort, NumPerPage];

export const RecipeFilterFields = [
  "Diet",
  "Meal Type",
  "Cuisine",
  "Sort",
  "Items Per Page",
];
