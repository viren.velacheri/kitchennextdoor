export default interface IRecipe {
  aggregateLikes: number;
  analyzedInstructions: any[];
  id: number;
  healthScore: number;
  type: string;
  dishTypes: string[];
  ingredients: any[];
  diets: string[];
  cuisines: string[];
  readyInMinutes: number;
  calories: number;
  pricePerServing: number;
  summary: string;
  title: string;
  sourceUrl: string;
  image: string;
  equipmentPics: string[];
}
export default interface IRecipeArray {
  [key: number]: IRecipe;
}

export const RECIPE_API = "http://localhost:5000/recipe";
