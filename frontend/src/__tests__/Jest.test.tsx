import React from "react";
import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import ReactDOM from "react-dom";

import App from "../App";
import Navbar from "../HomePage/Components/NavBar";
import About from "../Pages/About";
import PantryLanding from "../PantryPage/Components/PantryLanding";
import PantryCard from "../PantryPage/Components/PantryCard";
import RecipesLanding from "../RecipesPage/Components/RecipesLanding";
import RecipeInstance from "../RecipesPage/Components/RecipeInstance";
import StatsLanding from "../StatsPage/Components/StatsLanding";
import StatsInstance from "../StatsPage/Components/StatsInstance";

// Much of these tests were made using the following project as a reference:
// https://gitlab.com/JunLum/pride-in-writing/-/blob/main/front-end/src/__tests__/JestTests.test.tsx

describe("Render KitchenNextDoor Components", () => {
  // Test 1
  test("KitchenNextDoor Loads and Sets Document Title", () => {
    <BrowserRouter>
      render(
      <App />
      ); expect(global.window.document.title).toBe('KitchenNextDoor')
    </BrowserRouter>;
  });

  // Test 2
  test("Navbar Loads Correctly", () => {
    <BrowserRouter>
      render(
      <Navbar />
      ); expect(screen.getByText('About')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 3
  test("About Page Populates Team Data", () => {
    <BrowserRouter>
      render(
      <About />
      ); expect(screen.getByText('Viren Velacheri')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 4
  test("About Page Loads Tools Used", () => {
    <BrowserRouter>
      render(
      <About />
      ); expect(screen.getByText('React')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 5
  test("Pantry Landing Model Page Loads Correctly", () => {
    <BrowserRouter>
      render(
      <PantryLanding />
      ); expect(screen.getByText('Click for more details')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 6
  test("Recipes Landing Page Loads Correctly", () => {
    <BrowserRouter>
      render(
      <RecipesLanding />
      ); expect(screen.getByText('Find awesome recipes to prepare and
      donate')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 7
  test("Hunger Statistics Page Loads Correctly", () => {
    <BrowserRouter>
      render(
      <StatsLanding />
      ); expect(screen.queryByText('See the Effects of Hunger on
      Texas')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 8
  test("Pantry Instance Page Loads Correctly", () => {
    <BrowserRouter>
      render(
      <PantryCard />
      ); expect(screen.queryByText('Go somewhere')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 9
  test("Recipes Instance Page Loads Correctly", () => {
    <BrowserRouter>
      render(
      <RecipeInstance />
      ); expect(screen.queryByText('Meal')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 10
  test("Hunger Statistics Instance Page Loads Correctly", () => {
    <BrowserRouter>
      render(
      <StatsInstance />
      ); expect(screen.queryByText('Key Statistic')).toBeInTheDocument();
    </BrowserRouter>;
  });
});
