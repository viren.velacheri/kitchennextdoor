import { createTheme, ThemeProvider } from "@mui/material/styles";

const Theme = createTheme({
  palette: {
    primary: {
      light: "#e8e3dd",
      main: "#f9a826",
      dark: "#757575",
      contrastText: "#fff",
    },
    secondary: {
      // This is green.A700 as hex.
      main: "#11cb5f",
    },
  },
});

export default Theme;
