import React from "react";
import { API_ENDPOINT } from "../../GlobalConsts";
import axios from "axios";
import IRecipeArray from "../../RecipesPage/Interfaces/IRecipe";
import RecipeCard from "../../RecipesPage/Components/RecipeCard";
import IPantryArray from "../../PantryPage/Interfaces/IPantry";
import PantryCard from "../../PantryPage/Components/PantryCard";
import IStatsArray from "../../StatsPage/Interfaces/IStats";
import StatsCard from "../../StatsPage/Components/StatsCard";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import { ThemeProvider } from "@mui/material/styles";
import Theme from "../Interfaces/Theme";

import "../Style/SearchLanding.css";

function SearchLanding() {
  const [search, setSearch] = React.useState("");
  const [didMount, setDidMount] = React.useState(false);

  // Get search query
  React.useEffect(() => {
    const my_url = window.location.href;
    var parts = my_url.split("/");
    var result = parts[parts.length - 1];
    const url_search = result;

    setSearch(decodeURI(url_search));
    setDidMount(true);
  }, []);

  const [recipeData, setRecipeData] = React.useState<IRecipeArray[]>([]);
  const [pantryData, setPantryData] = React.useState<IPantryArray[]>([]);
  const [statsData, setStatsData] = React.useState<IStatsArray[]>([]);
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    if (didMount) {
      const local_url = "http://localhost:5000/search/" + search;
      const url = API_ENDPOINT + "search/" + search;

      console.log(url);
      axios
        .get(url)
        .then(function (response) {
          console.log(response);
          let recipeData = response.data.recipe;
          let statsData = response.data.county;
          let pantryData = response.data.bank;
          setRecipeData(recipeData);
          setPantryData(pantryData);
          setStatsData(statsData);

          setLoading(false);

          // handle succes
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }
  }, [search]);

  const getHighlightedText = (text: string, highlight: any) => {
    // Split on highlight term and include term into parts, ignore case
    const parts = text.split(new RegExp(`(${highlight})`, "gi"));
    return (
      <span>
        {" "}
        {parts.map((part: any, i: any) => (
          <span
            key={i}
            style={
              part.toLowerCase() === highlight.toLowerCase()
                ? { fontWeight: "bold" }
                : {}
            }
          >
            {part}
          </span>
        ))}{" "}
      </span>
    );
  };

  return (
    <div className="SearchBody">
      {loading ? (
        console.log("loading")
      ) : (
        <Typography variant="h1" gutterBottom component="div">
          Search Results - "{search}"
        </Typography>
      )}

      <Typography variant="h2" gutterBottom component="div">
        Recipes
      </Typography>

      {loading ? (
        console.log("loading")
      ) : (
        <Grid container spacing={2} alignItems="stretch">
          {recipeData.map(function (recipe) {
            return (
              <RecipeCard
                name={recipe.title}
                id={recipe.id}
                img_src={recipe.image}
                mealPrepTime={recipe.readyInMinutes}
                likes={recipe.aggregateLikes}
                calories={recipe.pricePerServing}
                healthScore={recipe.healthScore}
                search={search}
              />
            );
          })}
        </Grid>
      )}

      <div className="ButtonWrapper">
        <ThemeProvider theme={Theme}>
          <Button
            variant="contained"
            href="/recipes/view/"
            color="primary"
            disableElevation
          >
            More Recipes
          </Button>
        </ThemeProvider>
      </div>

      <div className="DividerWrapper">
        <Divider />
      </div>

      <Typography variant="h2" gutterBottom component="div">
        Counties
      </Typography>

      {loading ? (
        console.log("loading")
      ) : (
        <Grid container spacing={2} alignItems="stretch">
          {statsData.map(function (stat) {
            let img_url =
              "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1600&photo_reference=" +
              stat.photo +
              "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4";
            return (
              <StatsCard
                name={stat.county}
                id={stat.id}
                img_src={img_url}
                state={stat.state}
                costPerMeal={stat.costPerMeal}
                foodInsecurePeople={stat.foodInsecurePeople}
                foodInsecurityRate={stat.foodInsecurityRate}
                search={search}
              />
            );
          })}
        </Grid>
      )}
      <div className="ButtonWrapper">
        <ThemeProvider theme={Theme}>
          <Button
            variant="contained"
            href="/stats/view/"
            color="primary"
            disableElevation
          >
            More Counties
          </Button>
        </ThemeProvider>
      </div>
      <div className="DividerWrapper">
        <Divider />
      </div>

      <Typography variant="h2" gutterBottom component="div">
        Food Banks
      </Typography>

      {loading ? (
        console.log("loading")
      ) : (
        <Grid container spacing={2} alignItems="stretch">
          {pantryData.map(function (pantry) {
            let img_url =
              "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1600&photo_reference=" +
              pantry.photo +
              "&key=AIzaSyBn09fyxZtm8DC_7a702ymBlwqmWCaeog4";
            return (
              <PantryCard
                name={pantry.name}
                id={pantry.id}
                img_src={img_url}
                state={pantry.stateOrProvince}
                tagline={pantry.tagline}
                mission={pantry.mission}
                websiteURL={pantry.websiteURL}
                search={search}
              />
            );
          })}
        </Grid>
      )}
      <div className="ButtonWrapper">
        <ThemeProvider theme={Theme}>
          <Button
            variant="contained"
            href="/banks/view/"
            color="primary"
            disableElevation
          >
            More Food Banks
          </Button>
        </ThemeProvider>
      </div>
    </div>
  );
}

export default SearchLanding;
