import requests
import json
import sys

email_to_name_map = {"kjoseph3818@utexas.edu" : "Kevin Joseph", "toney.ejimadu@gmail.com" : "Antoney Ejimadu", 
            "viren.velacheri@utexas.edu" : "Viren Velacheri", "jmtran678@gmail.com" : "Michael Tran", 
            "nicholas.ehlers1@gmail.com" : "Nicholas Ehlers"}

username_to_name_map = {"kjoseph3818" : "Kevin Joseph", "toney.ejimadu" : "Antoney Ejimadu", 
            "viren.velacheri" : "Viren Velacheri", "jmtran678" : "Michael Tran", 
            "nicholas.ehlers1" : "Nicholas Ehlers"}

class commitInfo():
    def __init__(self, title, message, author, commit_date):
        self.title = title
        self.message = message
        self.author = author
        self.date = commit_date

    def dump(self):
        return { "Commit" : { 'title' : self.title,
                                'message' : self.message,
                                'author' : self.author,
                                'commit_date' : self.date}}

class issueInfo():
    def __init__(self, id, author, title, description, labels, open_date, close_date):
        self.id = id
        self.title = title
        self.description = description
        self.author = author
        self.labels = labels
        self.opened = open_date
        self.closed = close_date

    def dump(self):
        return { "Issue" : { 'id' : self.id,
                                'title' : self.title,
                                'description' : self.description,
                                'author' : self.author,
                                'labels' : self.labels,
                                'open_date' : self.opened,
                                'close_date' : self.closed}}

def getGitlabData():
    # Code for actual development
    
    commits_URL = "https://gitlab.com/api/v4/projects/34057057/repository/commits?ref_name=main"
    commits_r = requests.get(commits_URL)
    all_commits = commits_r.json()

    commit_map = {}
    total_commits = 0
    for commit in all_commits:
        author_name = email_to_name_map[commit['author_email']]
        if author_name in commit_map:
            commit_map[author_name] += 1
            total_commits += 1
        else:
            commit_map[author_name] = 1
            total_commits += 1
    commit_map["Total Commits"] = total_commits

    commit_json = json.dumps(commit_map)
    print(commit_json)

    issues_URL = "https://gitlab.com/api/v4/projects/34057057/issues"
    issues_r = requests.get(issues_URL)
    all_issues = issues_r.json()

    issues_map = {}
    total_issues = 0
    for issue in all_issues:
        if issue['author']['username'] in username_to_name_map:
            author_name = username_to_name_map[issue['author']['username']]
            if author_name in issues_map:
                issues_map[author_name] += 1
                total_issues += 1
            else:
                issues_map[author_name] = 1
                total_issues += 1
    issues_map["Total Issues"] = total_issues

    issue_json = json.dumps(issues_map)
    print(issue_json)
    sys.stdout.flush()



if __name__ == "__main__" :
    getGitlabData()
