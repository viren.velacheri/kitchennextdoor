import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import AboutCard from "./AboutPageCard";
import ToolsCard from "./ToolsCard";
import { Card, Button } from "react-bootstrap";

import antoney from "../Assets/antoney.jpg";
import viren from "../Assets/viren.jpg";
import nicholas from "../Assets/nicholas.jpg";
import michael from "../Assets/michael.jpg";
import kevin from "../Assets/kevin.jpg";
import postman from "../Assets/Postman.png";
import react_img from "../Assets/react.png";
import visual_studio from "../Assets/visualstudio.png";
import gitlab_img from "../Assets/gitlab.png";
import typescript_img from "../Assets/typescript.png";
import aws_img from "../Assets/aws.png";
import jest_img from "../Assets/jest.png";
import docker_img from "../Assets/docker.png";
import selenium_img from "../Assets/selenium.png";
import spoonacular_img from "../Assets/spoonacular.png";
import census_img from "../Assets/census.png";
import charity_img from "../Assets/charitynavigator.png";
import google_places_img from "../Assets/googleplaces.png";
import google_map_img from "../Assets/google_maps.png";

import "../Style/AboutLanding.css";
import { NavigationBar } from "../../HomePage/Components";
import { stringify } from "querystring";
import { Http2ServerResponse } from "http2";
import Axios from "axios";

var nameMap: { [key: string]: string } = {
  "Antoney Ejimadu": "toney.ejimadu",
  "Kevin Joseph": "kh3818",
  "kj3818": "kj3818",
  "Michael Tran": "jmtran678",
  "Nicholas Ehlers": "nicholas.ehlers1",
  "Viren Velacheri": "viren.velacheri",
  "viren-velacheri": "viren.velacheri"
};

let nameList = [
  "Kevin Joseph",
  "Viren Velacheri",
  "Antoney Ejimadu",
  "Nicholas Ehlers",
  "Michael Tran",
];
let idList = [
  "kj3818",
  "viren.velacheri",
  "toney.ejimadu",
  "nicholas.ehlers1",
  "jmtran678",
];

// var gitlab = "https://documenter.getpostman.com/view/19802851/UVksMEbE";

function retriveCommitData() {
  const [comments, setComments] = useState([]);
  useEffect(() => {
    fetchComments();
  }, []);

  const fetchComments = async () => {
    const response = await Axios.get(
      "https://gitlab.com/api/v4/projects/34057057/repository/contributors?order_by=name"
    );
    setComments(response.data);
  };
  var commitData: { [key: string]: number } = {};
  var data: { [key: number]: { [keyy: string]: string } } = comments;
  for (var idx in data) {
    var name = data[idx]["name"];
    if(commitData[nameMap[name]] == undefined){
      commitData[nameMap[name]] = parseInt(data[idx]["commits"], 10);
    } else {
      commitData[nameMap[name]] += parseInt(data[idx]["commits"], 10);
    } 
  }
  for(var name in commitData){
    console.log(name);
  }

  return commitData;
}

function retriveIssueData(i: number) {
  const [comments, setComments] = useState([]);
  var count = 0;

  useEffect(() => {
    fetchComments(i);
  }, []);

  count = 0;
  for (var temp in comments) {
    count = count + 1;
  }

  const fetchComments = async (i: number) => {
    const response = await Axios.get(
      "https://gitlab.com/api/v4/projects/34057057/issues?author_username=" +
        idList[i]
    );
    setComments(response.data);
  }; 
  return count;
}

function AboutPageLanding() {
  var issueData: { [key: string]: number } = {};
  for (let i = 0; i < 5; i++) {
    issueData[idList[i]] = retriveIssueData(i);
  }
  var commitData: { [key: string]: number } = retriveCommitData();
  var totalCommitData = commitData["viren.velacheri"] + commitData["jmtran678"] + commitData["nicholas.ehlers1"] + commitData["toney.ejimadu"] + commitData["kj3818"]
  var totalIssuesData = issueData["viren.velacheri"] + issueData["jmtran678"] + issueData["nicholas.ehlers1"] + issueData["toney.ejimadu"] + issueData["kj3818"]
  var virenC = (
    <AboutCard
      img_src={viren}
      name="Viren Velacheri"
      responsibilities="Front-End, Phase 1 Leader"
      bio="I am a 4th year CS major at UT Austin. I'm from Austin, Texas and in my free time I love playing various sports such as soccer, basketball, swimming, football, and tennis."
      commits={commitData["viren.velacheri"]}
      issues={issueData["viren.velacheri"]}
      unittests="27"
    />
  );

  var michaelC = (
    <AboutCard
      img_src={michael}
      name="Michael Tran"
      responsibilities="Back-End, Phase 2 Leader"
      bio="I am a 3rd year CS Major at UT Austin. I am originally from Philadelphia (Go Birds!) and grew up in Killeen, Texas right by Fort Hood. Outside of class I am involved in a lot of musical activities like Healing with Harmonies and UT Conjunto."
      commits={commitData["jmtran678"]}
      issues={issueData["jmtran678"]}
      unittests="0"
    />
  );

  var nicholasC = (
    <AboutCard
      img_src={nicholas}
      name="Nicholas Ehlers"
      responsibilities="Front-End, Phase 4 Leader"
      bio="I am a 4th year CS major at UT Austin. While I am originally from Huntington Beach, CA, I have spent most of my life in Dallas, TX, so I consider myself a Texan now. Outside of class, I enjoy hiking, hanging out with friends, and binge-watching."
      commits={commitData["nicholas.ehlers1"]}
      issues={issueData["nicholas.ehlers1"]}
      unittests="0"
    />
  );

  var antoneyC = (
    <AboutCard
      img_src={antoney}
      name="Antoney Ejimadu"
      responsibilities="Back-End, Phase 3 Leader"
      bio="I am a 3rd year CS major at UT Austin. I was born in Houston, Texas, but my family moved to Sugar Land when I was really young. Outside of school I enjoy listening to music, as well as learning music, and watching TV and movies."
      commits={commitData["toney.ejimadu"]}
      issues={issueData["toney.ejimadu"]}
      unittests="0"
    />
  );

  var kevinC = (
    <AboutCard
      img_src={kevin}
      name="Kevin Joseph"
      responsibilities="Front-End"
      bio="I am a 3rd year CS major at UT Austin. I grew up in Sugar Land, Texas near Houston, but my family now lives in Austin. Outside of school, I enjoy activities such as hiking, playing games, and playing the piano."
      commits={commitData["kj3818"]}
      issues={issueData["kj3818"]}
      unittests="10"
    />
  );

  var postmanC = (
    <ToolsCard
      img_src={postman}
      name="Postman API Documentation"
      use=""
      link="https://documenter.getpostman.com/view/19802851/UVksMEbE"
    />
  );

  var postmanDoc = (
    <ToolsCard
      img_src={postman}
      name="Postman"
      use="We used Postman to document our API."
      link="https://learning.postman.com/docs/getting-started/introduction/"
    />
  );

  var react = (
    <ToolsCard
      img_src={react_img}
      name="React"
      use="We used React for the front-end development."
      link="https://reactjs.org/"
    />
  );

  var visualstudio = (
    <ToolsCard
      img_src={visual_studio}
      name="Visual Studio"
      use="Visual Studio was our main IDE."
      link="https://visualstudio.microsoft.com/"
    />
  );

  var gitlabDoc = (
    <ToolsCard
      img_src={gitlab_img}
      name="Gitlab"
      use="We used Gitlab for version control."
      link="https://about.gitlab.com/"
    />
  );

  var gitlabC = (
    <ToolsCard
      img_src={gitlab_img}
      name="Gitlab Repository"
      use=""
      link="https://gitlab.com/viren.velacheri/kitchennextdoor/-/tree/main"
    />
  );

  var aws = (
    <ToolsCard
      img_src={aws_img}
      name="AWS"
      use="We used AWS for cloud hosting purposes."
      link="https://aws.amazon.com/"
    />
  );

  var docker = (
    <ToolsCard
      img_src={docker_img}
      name="Docker"
      use="We used Docker for creating stable runtime environments."
      link="https://www.docker.com/"
    />
  );

  var typescript = (
    <ToolsCard
      img_src={typescript_img}
      name="Typescript"
      use="We used Typescript as programming language for React."
      link="https://www.typescriptlang.org/"
    />
  );

  var jest = (
    <ToolsCard
      img_src={jest_img}
      name="Jest"
      use="We used Jest for testing Typescript files"
      link="https://jestjs.io/"
    />
  );

  var selenium = (
    <ToolsCard
      img_src={selenium_img}
      name="Selenium"
      use="We used Selenium as our UI Testing Framework."
      link="https://www.selenium.dev/"
    />
  );

  var census = (
    <ToolsCard
      img_src={census_img}
      name="Census Bureau API"
      use="Used for collecting data for Statistics Model."
      link="https://www.census.gov/data/developers.html"
    />
  );

  var spoonacular = (
    <ToolsCard
      img_src={spoonacular_img}
      name="Spoonacular API"
      use="Used for collecting data for Recipes Model."
      link="https://spoonacular.com/food-api"
    />
  );

  var charity = (
    <ToolsCard
      img_src={charity_img}
      name="Charity Navigator API"
      use="Used for collecting data for Pantries Model."
      link="https://www.charitynavigator.org/index.cfm?bay=content.view&cpid=1397"
    />
  );

  var googleplaces = (
    <ToolsCard
      img_src={google_places_img}
      name="Google Places API"
      use="Used for getting location images for Pantry and County Instances."
      link="https://developers.google.com/maps/documentation/places/web-service/overview"
    />
  );

  var googlemap = (
    <ToolsCard
      img_src={google_map_img}
      name="Google Maps API"
      use="Used for creating map with marker given the location coordinates for Pantry and County Places."
      link="https://developers.google.com/maps/documentation/places/web-service/overview"
    />
  );

  return (
    <div className="Div">
      <div className="intro">
        <p className="bigText">About Us</p>
        <p className="desc">
          KitchenNextDoor is an all inclusive gateway towards helping those in
          need of food. Users can quickly look up nearby food banks based on
          location, and they are also presented with information on how
          prevalent hunger is in the nation. Finally, users can also access a
          list of easy recipes to make or donate. We hope to increase awareness
          for this systemic problem in the country as well as educate people on
          what they can do to give or get help.{" "}
        </p>
        <Container>
          <p className="bigText">Team Members</p>
          <Row>
            <Col>{virenC}</Col>
            <Col>{michaelC}</Col>
            <Col>{nicholasC}</Col>
            <Col>{antoneyC}</Col>
            <Col>{kevinC}</Col>
          </Row>
          <p className="bigText">Repository Statistics</p>
          <Row>
            <Col>
              <Card>
                <Card.Body>
                  <Card.Title>Total Commits</Card.Title>
                  {totalCommitData}
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card>
                <Card.Body>
                  <Card.Title>Issues</Card.Title>
                  {totalIssuesData}
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card>
                <Card.Body>
                  <Card.Title>Unit Tests</Card.Title>
                  37
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <p className="bigText">Tools Utilized</p>
          <Row>
            <Col>{postmanDoc}</Col>
            <Col>{visualstudio}</Col>
            <Col>{react}</Col>
            <Col>{gitlabDoc}</Col>
          </Row>
          <Row>
            <Col>{aws}</Col>
            <Col>{docker}</Col>
          </Row>
          <Row>
            <Col>{jest}</Col>
            <Col>{selenium}</Col>
            <Col>{typescript}</Col>
          </Row>
          <p className="bigText">Data Sources Utilized</p>
          <Row>
            <Col>{census}</Col>
            <Col>{spoonacular}</Col>
            <Col>{charity}</Col>
            <Col>{googleplaces}</Col>
            <Col>{googlemap}</Col>
          </Row>
          <p className="bigText"> GitLab Repository and Postman API</p>
          <Row>
            <Col>{gitlabC}</Col>
            <Col>{postmanC}</Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}
export default AboutPageLanding;
