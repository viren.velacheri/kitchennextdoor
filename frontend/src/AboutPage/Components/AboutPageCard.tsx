import React from "react";
import { Card, Button } from "react-bootstrap";
import { PassThrough } from "stream";
import "../Style/AboutCard.css";

interface Props {
  img_src?: string;
  name?: string;
  commits?: number;
  issues?: number;
  unittests?: string;
  bio?: string;
  responsibilities?: string;
}

const AboutCard = (props: Props) => {
  return (
    <Card className="card">
      <Card.Img variant="top" src={props.img_src} className="cardImage" />
      <Card.Body>
        <Card.Title>{props.name}</Card.Title>
        <Card.Text className="roleText"> {props.responsibilities}</Card.Text>
        <Card.Text> {"Commits: " + props.commits}</Card.Text>
        <Card.Text> {"Issues: " + props.issues}</Card.Text>
        <Card.Text> {"Unit Tests: " + props.unittests}</Card.Text>

        <Card.Text> {props.bio}</Card.Text>
      </Card.Body>
    </Card>
  );
};

export default AboutCard;
