import React from "react";
import { Card, Button } from "react-bootstrap";
import { PassThrough } from "stream";
import "../Style/AboutCard.css";

interface Props {
  img_src?: string;
  name?: string;
  link?: string;
  use?: string;
}

const ToolsCard = (props: Props) => {
  return (
    <a href={props.link}>
      <Card className={"card"}>
        <Card.Img variant="top" src={props.img_src} className="cardImage" />
        <Card.Body>
          <Card.Title>{props.name}</Card.Title>
          <Card.Text> {props.use}</Card.Text>
        </Card.Body>
      </Card>
    </a>
  );
};

export default ToolsCard;
