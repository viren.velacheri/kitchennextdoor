import * as React from "react";
import FilterSelect from "./FilterButton";
import { Container, Row, Col } from "react-bootstrap";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";

interface IProps {
  fields?: string[];
  fieldChoices?: any[][];
  // Lazy workaround to pass onChange function as prop
  onChange?: (e: any, g: any) => void;
}

export default function FilterBar(props: IProps) {
  // State Values to hold filter vals
  const [field1, setField1] = React.useState("");
  const [field2, setField2] = React.useState("");
  const [field3, setField3] = React.useState("");
  const [field4, setField4] = React.useState("");
  const [field5, setField5] = React.useState("");

  // Used to get filter vals from child component
  function eventHandler(data: string, id: number) {
    switch (id) {
      case 1:
        setField1(data);
        break;
      case 2:
        setField2(data);
        break;
      case 3:
        setField3(data);
        break;
      case 4:
        setField4(data);
        break;
      case 5:
        setField5(data);
        break;
      default:
        console.log("DEFAULT");
        break;
    }
  }

  const [search, setSearch] = React.useState("");
  const [tempSearch, setTemp] = React.useState("");
  const handleSearch = (val: string) => {
    setSearch(val);
  };
  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTemp(event.target.value);
  };

  // Listeners that wait for state vals to get updated
  React.useEffect(() => {
    if (props.onChange) {
      props.onChange(field1, 1);
    }
  }, [field1]);

  React.useEffect(() => {
    if (props.onChange) {
      props.onChange(field2, 2);
    }
  }, [field2]);

  React.useEffect(() => {
    if (props.onChange) {
      props.onChange(field3, 3);
    }
  }, [field3]);

  React.useEffect(() => {
    if (props.onChange) {
      props.onChange(field4, 4);
    }
  }, [field4]);

  React.useEffect(() => {
    if (props.onChange) {
      props.onChange(field5, 5);
    }
  }, [field5]);

  React.useEffect(() => {
    if (props.onChange) {
      console.log("SEND SEARCH" + search);
      props.onChange(search, 6);
    }
  }, [search]);

  const filterFields = props.fields || [];
  const filterChoices = props.fieldChoices || [];

  // Need 5 filters in total and 1 search field
  return (
    <Container>
      <Row>
        {filterFields.map((item, index) => {
          return (
            <Col>
              <FilterSelect
                id={index + 1}
                field={item}
                categories={filterChoices[index]}
                onChange={eventHandler}
              />
            </Col>
          );
        })}
        <Col>
          <form noValidate autoComplete="off">
            <TextField
              id="standard-basic"
              label="Search Items"
              onKeyPress={(e) => {
                if (e.key === "Enter") {
                  console.log("ENTER");
                  console.log((e.target as HTMLTextAreaElement).value);
                  handleSearch((e.target as HTMLTextAreaElement).value);
                  e.preventDefault();
                }
              }}
            />
          </form>
        </Col>
      </Row>
    </Container>
  );
}
