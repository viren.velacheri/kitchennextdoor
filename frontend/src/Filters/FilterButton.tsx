import * as React from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";

interface IProp {
  id?: number;
  field?: string;
  categories?: any[];
  onChange?: (e: any, g: any) => void;
}

// From MUI BasicSelect
export default function FilterSelect(props: IProp) {
  const [val, setVal] = React.useState("");
  const handleChange = (event: SelectChangeEvent) => {
    setVal(event.target.value as string);
  };

  React.useEffect(() => {
    if (props.onChange) {
      props.onChange(val, props.id);
    }
  }, [val]);

  const filterFields = props.categories || [];

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">{props.field}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={val}
          label={props.field}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {filterFields.map(({ name, value }) => (
            <MenuItem value={value}>{name}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}
